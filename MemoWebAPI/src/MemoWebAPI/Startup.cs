﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using HoloLC.MemoDataLib.Managers;
using HoloLC.MemoDataLib.Managers.Impl;
using HoloLC.MemoDataLib.DB;
using Microsoft.AspNetCore.Authorization;
using HoloLC.MemoWebAPI.Auth;
using Microsoft.EntityFrameworkCore;
using NLog.Extensions.Logging;
using Amazon.DynamoDBv2;
using HoloLC.MemoDataLib.Dynamo.Tables;
using HoloLC.MemoDataLib.Registries;

namespace HoloLC.MemoWebAPI
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            // ???
            //using(var db = new PickupDbContnext()) {
            //    db.Database.EnsureCreated();
            //}

            Configuration = builder.Build();
            // var val = Configuration.GetValue<string>("key");
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Testing...
            services.AddAuthorization(options => {
                // options.AddPolicy("APIClientOnly", policy => policy.RequireRole("APIClientRole"));
                // options.AddPolicy("PickupCenterManager", policy => policy.RequireClaim("PickupCenterManager"));
                options.AddPolicy("APIClientOnly", policy => policy.Requirements.Add(new APIClientRequirement()));
            });

            // Add framework services.
            services.AddMvc(config => {
                //var policy = new AuthorizationPolicyBuilder()
                // // tbd:
                //. RequireAuthenticatedUser()
                //.Build();
                //config.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddLogging();

            // Swagger support
            services.AddSwaggerGen();

            // TBD:
            // var connection = Configuration["DataProvider:SQLiteConnectionString"];
            // ....
            var dbDir = Environment.GetEnvironmentVariable("SQLITE_DB_DIR");
            if (dbDir == null) {
                dbDir = "";
            }
            // Note that dbDir should end with "/" or "\\" if it's not null/empty.
            var dbFilePath = dbDir + Configuration["DataProvider:SQLiteDBFile"];
            System.Diagnostics.Debug.WriteLine($">>> SQLite db file path: {dbFilePath}");
            var connection = $"Data Source={dbFilePath}";
            // ....
            services.AddDbContext<MemoDbContext>(options => options.UseSqlite(connection, b => b.MigrationsAssembly("memowebapi")));
            // services.AddDbContext<MemoDbContext>(options => options.UseSqlite(connection, b => b.MigrationsAssembly("MemoWebAPI")));
            // services.AddEntityFrameworkSqlite().AddDbContext<PickupDbContnext>();
            // ...

            // TBD:
            //services.AddSingleton<IRepositoryManager, MemoryRepositoryManager>();
            //// services.AddScoped<IRepositoryManager, MemoryRepositoryManager>(); 
            // ....
            //// services.AddSingleton<IRepositoryManager, MemoryRepositoryManager>();
            //services.AddScoped<IRepositoryManager, PersistentRepositoryManager>();
            // TBD


            //// temporary
            //AmazonDynamoDBClient client = new AmazonDynamoDBClient();   // tbd
            //IDynamoDBClientContext clientContext = new DefaultDynamoDBClientContext(client);
            //ITableCollection tableCollection = new BaseTableCollection();
            //tableCollection.SetTable(DynamoBinderChildTable.TABLE_KEY, new DynamoBinderChildTable(clientContext, $"{DynamoBinderChildTable.TABLE_KEY}.Devel"));
            //tableCollection.SetTable(DynamoBoardChildTable.TABLE_KEY, new DynamoBoardChildTable(clientContext, $"{DynamoBoardChildTable.TABLE_KEY}.Devel"));
            //tableCollection.SetTable(DynamoMemoBinderTable.TABLE_KEY, new DynamoMemoBinderTable(clientContext, $"{DynamoMemoBinderTable.TABLE_KEY}.Devel"));
            //tableCollection.SetTable(DynamoMemoBoardTable.TABLE_KEY, new DynamoMemoBoardTable(clientContext, $"{DynamoMemoBoardTable.TABLE_KEY}.Devel"));
            //tableCollection.SetTable(DynamoMemoLayerTable.TABLE_KEY, new DynamoMemoLayerTable(clientContext, $"{DynamoMemoLayerTable.TABLE_KEY}.Devel"));
            //tableCollection.SetTable(DynamoMemoShardTable.TABLE_KEY, new DynamoMemoShardTable(clientContext, $"{DynamoMemoShardTable.TABLE_KEY}.Devel"));
            //IRepositoryManager repositoryManager = new DynamoRepositoryManager(tableCollection);
            //services.AddSingleton(repositoryManager);
            //// temporary

            // temporary
            var dynamoServiceURL = Configuration["DynamoDBService:ServiceURL"];
            var schemaVersion = Configuration["DynamoDBService:SchemaVersion"];
            var envSuffix = Configuration["DynamoDBService:EnvSuffix"];
            var tableSuffix = $"V{schemaVersion}{((envSuffix != null && envSuffix.Any()) ? "." + envSuffix : "")}";
            var tableCollection = TableCollectionRegistry.Instance.GetTableCollection(dynamoServiceURL, tableSuffix);
            IDataRepositoryManager repositoryManager = new DynamoRepositoryManager(tableCollection);
            //try {
            //    // Create tables only if they do not exist.
            //    /* await */
            //    (repositoryManager as IInstantRepositoryManager)?.CreateAllAsync().Wait();
            //    // await Task.Delay(3000);
            //} catch(Exception ex1)
            //{
            //    // ignore. Tables should have been created before the app starts anyway.
            //    System.Diagnostics.Debug.WriteLine($"Failed to create tables. Ex = {ex1.Message}");
            //}
            services.AddSingleton(repositoryManager);
            // temporary


            // For auth.
            services.AddSingleton<IAuthorizationHandler, BasicAuthHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            loggerFactory.AddNLog();
            env.ConfigureNLog($"nlog.{env.EnvironmentName}.config");

            // TBD:
            if (env.IsDevelopment()) {
                // ...
            } else if (env.IsProduction()) {
                // ...
            } else {
                // ...
            }

            app.UseMvc();

            // Swagger support
            app.UseSwagger();
            app.UseSwaggerUi();

            // Serve HTML pages...
            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
