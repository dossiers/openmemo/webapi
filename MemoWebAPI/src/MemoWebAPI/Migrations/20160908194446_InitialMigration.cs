﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace memowebapi.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ElementPosition",
                columns: table => new
                {
                    X = table.Column<int>(nullable: false),
                    Y = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElementPosition", x => new { x.X, x.Y });
                });

            migrationBuilder.CreateTable(
                name: "BinderChildren",
                columns: table => new
                {
                    BinderId = table.Column<ulong>(nullable: false),
                    BoardId = table.Column<ulong>(nullable: false),
                    BoardTitle = table.Column<string>(nullable: true),
                    BoardZOrder = table.Column<int>(nullable: false),
                    Created = table.Column<long>(nullable: false),
                    Deleted = table.Column<long>(nullable: false),
                    Updated = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BinderChildren", x => new { x.BinderId, x.BoardId });
                });

            migrationBuilder.CreateTable(
                name: "MemoBinders",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Autoincrement", true),
                    AccessLevel = table.Column<int>(nullable: false),
                    Created = table.Column<long>(nullable: false),
                    Deleted = table.Column<long>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Height = table.Column<uint>(nullable: false),
                    HomeBoard = table.Column<ulong>(nullable: false),
                    Owner = table.Column<ulong>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Updated = table.Column<long>(nullable: false),
                    Width = table.Column<uint>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemoBinders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MemoBoards",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Autoincrement", true),
                    AccessLevel = table.Column<int>(nullable: false),
                    Created = table.Column<long>(nullable: false),
                    Deleted = table.Column<long>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Height = table.Column<uint>(nullable: false),
                    IsTerminal = table.Column<bool>(nullable: false),
                    Label = table.Column<string>(nullable: true),
                    Owner = table.Column<ulong>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Updated = table.Column<long>(nullable: false),
                    Width = table.Column<uint>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemoBoards", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MemoLayers",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Autoincrement", true),
                    AccessLevel = table.Column<int>(nullable: false),
                    Board = table.Column<ulong>(nullable: false),
                    Created = table.Column<long>(nullable: false),
                    Deleted = table.Column<long>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Height = table.Column<uint>(nullable: false),
                    Owner = table.Column<ulong>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Updated = table.Column<long>(nullable: false),
                    Width = table.Column<uint>(nullable: false),
                    ZOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemoLayers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BoardChildren",
                columns: table => new
                {
                    ParentId = table.Column<ulong>(nullable: false),
                    ChildId = table.Column<ulong>(nullable: false),
                    ChildPositionX = table.Column<int>(nullable: true),
                    ChildPositionY = table.Column<int>(nullable: true),
                    ChildTitle = table.Column<string>(nullable: true),
                    ChildZOrder = table.Column<int>(nullable: false),
                    Created = table.Column<long>(nullable: false),
                    Deleted = table.Column<long>(nullable: false),
                    Updated = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoardChildren", x => new { x.ParentId, x.ChildId });
                    table.ForeignKey(
                        name: "FK_BoardChildren_ElementPosition_ChildPositionX_ChildPositionY",
                        columns: x => new { x.ChildPositionX, x.ChildPositionY },
                        principalTable: "ElementPosition",
                        principalColumns: new[] { "X", "Y" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MemoShards",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Autoincrement", true),
                    Board = table.Column<ulong>(nullable: false),
                    Created = table.Column<long>(nullable: false),
                    Deleted = table.Column<long>(nullable: false),
                    Layer = table.Column<ulong>(nullable: false),
                    PositionX = table.Column<int>(nullable: true),
                    PositionY = table.Column<int>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Updated = table.Column<long>(nullable: false),
                    ZOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemoShards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemoShards_ElementPosition_PositionX_PositionY",
                        columns: x => new { x.PositionX, x.PositionY },
                        principalTable: "ElementPosition",
                        principalColumns: new[] { "X", "Y" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BoardChildren_ChildPositionX_ChildPositionY",
                table: "BoardChildren",
                columns: new[] { "ChildPositionX", "ChildPositionY" });

            migrationBuilder.CreateIndex(
                name: "IX_MemoShards_PositionX_PositionY",
                table: "MemoShards",
                columns: new[] { "PositionX", "PositionY" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BinderChildren");

            migrationBuilder.DropTable(
                name: "BoardChildren");

            migrationBuilder.DropTable(
                name: "MemoBinders");

            migrationBuilder.DropTable(
                name: "MemoBoards");

            migrationBuilder.DropTable(
                name: "MemoLayers");

            migrationBuilder.DropTable(
                name: "MemoShards");

            migrationBuilder.DropTable(
                name: "ElementPosition");
        }
    }
}
