﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using HoloLC.MemoDataLib.DB;

namespace memowebapi.Migrations
{
    [DbContext(typeof(MemoDbContext))]
    [Migration("20160908194446_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("HoloLC.MemoCoreLib.Common.ElementPosition", b =>
                {
                    b.Property<int>("X");

                    b.Property<int>("Y");

                    b.HasKey("X", "Y");

                    b.ToTable("ElementPosition");
                });

            modelBuilder.Entity("HoloLC.MemoCoreLib.Models.BinderChild", b =>
                {
                    b.Property<ulong>("BinderId");

                    b.Property<ulong>("BoardId");

                    b.Property<string>("BoardTitle");

                    b.Property<int>("BoardZOrder");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<long>("Updated");

                    b.HasKey("BinderId", "BoardId");

                    b.ToTable("BinderChildren");
                });

            modelBuilder.Entity("HoloLC.MemoCoreLib.Models.BoardChild", b =>
                {
                    b.Property<ulong>("ParentId");

                    b.Property<ulong>("ChildId");

                    b.Property<int?>("ChildPositionX");

                    b.Property<int?>("ChildPositionY");

                    b.Property<string>("ChildTitle");

                    b.Property<int>("ChildZOrder");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<long>("Updated");

                    b.HasKey("ParentId", "ChildId");

                    b.HasIndex("ChildPositionX", "ChildPositionY");

                    b.ToTable("BoardChildren");
                });

            modelBuilder.Entity("HoloLC.MemoCoreLib.Models.MemoBinder", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessLevel");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<string>("Description");

                    b.Property<uint>("Height");

                    b.Property<ulong>("HomeBoard");

                    b.Property<ulong>("Owner");

                    b.Property<string>("Title");

                    b.Property<long>("Updated");

                    b.Property<uint>("Width");

                    b.HasKey("Id");

                    b.ToTable("MemoBinders");
                });

            modelBuilder.Entity("HoloLC.MemoCoreLib.Models.MemoBoard", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessLevel");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<string>("Description");

                    b.Property<uint>("Height");

                    b.Property<bool>("IsTerminal");

                    b.Property<string>("Label");

                    b.Property<ulong>("Owner");

                    b.Property<string>("Title");

                    b.Property<long>("Updated");

                    b.Property<uint>("Width");

                    b.HasKey("Id");

                    b.ToTable("MemoBoards");
                });

            modelBuilder.Entity("HoloLC.MemoCoreLib.Models.MemoLayer", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessLevel");

                    b.Property<ulong>("Board");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<string>("Description");

                    b.Property<uint>("Height");

                    b.Property<ulong>("Owner");

                    b.Property<string>("Title");

                    b.Property<long>("Updated");

                    b.Property<uint>("Width");

                    b.Property<int>("ZOrder");

                    b.HasKey("Id");

                    b.ToTable("MemoLayers");
                });

            modelBuilder.Entity("HoloLC.MemoCoreLib.Models.MemoShard", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<ulong>("Board");

                    b.Property<long>("Created");

                    b.Property<long>("Deleted");

                    b.Property<ulong>("Layer");

                    b.Property<int?>("PositionX");

                    b.Property<int?>("PositionY");

                    b.Property<int>("Type");

                    b.Property<long>("Updated");

                    b.Property<int>("ZOrder");

                    b.HasKey("Id");

                    b.HasIndex("PositionX", "PositionY");

                    b.ToTable("MemoShards");
                });

            modelBuilder.Entity("HoloLC.MemoCoreLib.Models.BoardChild", b =>
                {
                    b.HasOne("HoloLC.MemoCoreLib.Common.ElementPosition", "ChildPosition")
                        .WithMany()
                        .HasForeignKey("ChildPositionX", "ChildPositionY");
                });

            modelBuilder.Entity("HoloLC.MemoCoreLib.Models.MemoShard", b =>
                {
                    b.HasOne("HoloLC.MemoCoreLib.Common.ElementPosition", "Position")
                        .WithMany()
                        .HasForeignKey("PositionX", "PositionY");
                });
        }
    }
}
