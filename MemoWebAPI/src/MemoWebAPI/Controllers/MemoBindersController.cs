﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.MemoDataLib.Managers;
using HoloLC.MemoDataLib.Repositories;
using HoloLC.MemoCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.MemoWebAPI.Controllers
{
    [Route("api/user/{owner}/[controller]")]
    public class MemoBindersController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MemoBindersController(IDataRepositoryManager repositoryManager)
        {
            MemoBinderRepository = repositoryManager.MemoBinderRepository;
        }
        public IMemoBinderRepository MemoBinderRepository { get; set; }

        [HttpGet]
        public async Task<IEnumerable<MemoBinder>> FindAllAsync(ulong owner = 0UL)
        {
            var binders = await MemoBinderRepository.FindAllByOwnerAsync(owner);
            return binders;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(ulong id, ulong owner = 0UL)
        {
            var fetched = await MemoBinderRepository.GetItemAsync(id, owner);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public async Task PostAsync([FromBody] MemoBinder binder, ulong owner = 0UL)
        {
            var suc = await MemoBinderRepository.AddItemAsync(binder);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public async Task PutAsync(ulong id, [FromBody] MemoBinder binder)
        {
            var suc = await MemoBinderRepository.ReplaceItemAsync(binder, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong owner)
        {
            return await MemoBinderRepository.RemoveAllByOwnerAsync(owner);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(ulong id, ulong owner = 0UL)
        {
            var deleted = await MemoBinderRepository.DeleteItemAsync(id, owner);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}