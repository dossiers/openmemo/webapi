﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.MemoDataLib.Managers;
using HoloLC.MemoDataLib.Repositories;
using HoloLC.MemoCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.MemoWebAPI.Controllers
{
    [Route("api/layer/{layer}/[controller]")]
    public class MemoShardsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MemoShardsController(IDataRepositoryManager repositoryManager)
        {
            MemoShardRepository = repositoryManager.MemoShardRepository;
        }
        public IMemoShardRepository MemoShardRepository { get; set; }

        [HttpGet]
        public async Task<IEnumerable<MemoShard>> FindAllAsync(ulong layer = 0UL)
        {
            var shards = await MemoShardRepository.FindAllByLayerAsync(layer);
            return shards;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(ulong id, ulong layer = 0UL)
        {
            var fetched = await MemoShardRepository.GetItemByLayerAsync(id, layer);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public async Task PostAsync([FromBody] MemoShard shard, ulong layer = 0UL)
        {
            var suc = await MemoShardRepository.AddItemAsync(shard);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public async Task PutAsync(ulong id, [FromBody] MemoShard shard)
        {
            var suc = await MemoShardRepository.ReplaceItemAsync(shard, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong layer)
        {
            return await MemoShardRepository.RemoveAllByLayerAsync(layer);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(ulong id, ulong layer = 0UL)
        {
            var deleted = await MemoShardRepository.DeleteItemByLayerAsync(id, layer);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
