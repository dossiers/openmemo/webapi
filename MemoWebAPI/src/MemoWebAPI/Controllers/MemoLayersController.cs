﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.MemoDataLib.Managers;
using HoloLC.MemoDataLib.Repositories;
using HoloLC.MemoCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.MemoWebAPI.Controllers
{
    [Route("api/board/{board}/[controller]")]
    public class MemoLayersController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MemoLayersController(IDataRepositoryManager repositoryManager)
        {
            MemoLayerRepository = repositoryManager.MemoLayerRepository;
        }
        public IMemoLayerRepository MemoLayerRepository { get; set; }

        [HttpGet]
        public async Task<IEnumerable<MemoLayer>> FindAllAsync(ulong board = 0UL)
        {
            var layers = await MemoLayerRepository.FindAllByBoardAsync(board);
            return layers;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(ulong id, ulong board = 0UL)
        {
            var fetched = await MemoLayerRepository.GetItemByBoardAsync(id, board);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public async Task PostAsync([FromBody] MemoLayer layer, ulong board = 0UL)
        {
            var suc = await MemoLayerRepository.AddItemAsync(layer);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public async Task PutAsync(ulong id, [FromBody] MemoLayer layer)
        {
            var suc = await MemoLayerRepository.ReplaceItemAsync(layer, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong board)
        {
            return await MemoLayerRepository.RemoveAllByBoardAsync(board);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(ulong id, ulong board = 0UL)
        {
            var deleted = await MemoLayerRepository.DeleteItemByBoardAsync(id, board);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
