﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.MemoDataLib.Managers;
using HoloLC.MemoDataLib.Repositories;
using HoloLC.MemoCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.MemoWebAPI.Controllers
{
    [Route("api/user/{owner}/[controller]")]
    public class MemoBoardsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MemoBoardsController(IDataRepositoryManager repositoryManager)
        {
            MemoBoardRepository = repositoryManager.MemoBoardRepository;
        }
        public IMemoBoardRepository MemoBoardRepository { get; set; }

        [HttpGet]
        public async Task<IEnumerable<MemoBoard>> FindAllAsync(ulong owner = 0UL)
        {
            var boards = await MemoBoardRepository.FindAllByOwnerAsync(owner);
            return boards;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(ulong id, ulong owner = 0UL)
        {
            var fetched = await MemoBoardRepository.GetItemAsync(id, owner);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public async Task PostAsync([FromBody] MemoBoard board, ulong owner = 0UL)
        {
            var suc = await MemoBoardRepository.AddItemAsync(board);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public async Task PutAsync(ulong id, [FromBody] MemoBoard board)
        {
            var suc = await MemoBoardRepository.ReplaceItemAsync(board, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong owner)
        {
            return await MemoBoardRepository.RemoveAllByOwnerAsync(owner);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(ulong id, ulong owner = 0UL)
        {
            var deleted = await MemoBoardRepository.DeleteItemAsync(id, owner);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
