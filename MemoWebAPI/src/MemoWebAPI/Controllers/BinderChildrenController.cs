﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HoloLC.MemoDataLib.Managers;
using HoloLC.MemoDataLib.Repositories;
using HoloLC.MemoCoreLib.Models;
using NLog;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HoloLC.MemoWebAPI.Controllers
{
    // [Route("api/board/{binderId}/board/{binderChild}")]
    [Route("api/[controller]")]
    public class BinderChildrenController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public BinderChildrenController(IDataRepositoryManager repositoryManager)
        {
            BinderChildRepository = repositoryManager.BinderChildRepository;
        }
        public IBinderChildRepository BinderChildRepository { get; set; }


        [HttpGet("binderId/{binderId}")]
        public async Task<IEnumerable<BinderChild>> FindChildren(ulong binderId)
        {
            var binderChildfren = await BinderChildRepository.FindMemoBoardsAsync(binderId);
            return binderChildfren;
        }

        [HttpPost]
        public async Task PostAsync([FromBody] BinderChild binderChild)
        {
            var suc = await BinderChildRepository.AddBinderChildAsync(binderChild);

            // tbd:
            if (suc) {

            } else {

            }
        }

        //[HttpPut("{id}")]
        //public async Task<void Put(ulong id, [FromBody] BinderChild binderChild)
        //{
        //    var suc = BinderChildRepository.ReplaceBinderChild(binderChild, true);
        //    // tbd:
        //    if (suc) {
        //    } else {
        //    }
        //}

        [HttpPut("board/{binderIdId}/board/{binderChildId}")]
        public async Task PutAsync(ulong binderIdId, ulong binderChildId, [FromBody] BinderChild binderChild)
        {
            // assert binderId == binderChild.ParentId && binderChild = binderChild.ChildId
            var suc = await BinderChildRepository.ReplaceBinderChildAsync(binderChild, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public async Task<int> RemoveAllAsync(ulong binderId)
        {
            return await BinderChildRepository.RemoveByBinderAsync(binderId);
        }

        //[HttpDelete("{id}")]
        //public void Delete(ulong binderIdId, ulong binderChildId)
        //{
        //    BinderChildRepository.DeleteBinderChild(id);
        //}
        [HttpDelete("board/{binderIdId}/board/{binderChildId}")]
        public async Task<IActionResult> DeleteAsync(ulong binderIdId, ulong binderChildId)
        {
            var deleted = await BinderChildRepository.DeleteBinderChildAsync(binderIdId, binderChildId);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
