﻿using Microsoft.AspNetCore.Authorization;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoWebAPI.Auth
{
    // Cf: https://github.com/blowdart/AspNetAuthorizationWorkshop
    public class BasicAuthHandler : AuthorizationHandler<APIClientRequirement>
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, APIClientRequirement requirement)
        {
            // tbd:

            // Do basic auth for API
            // or, do claims check for pickup center managers..   --> this may not be needed. Just use APIClient....
            // ...


            // just testing...
            context.Succeed(requirement);
            // context.Fail();
            // just testing...

            return Task.FromResult(0);
        }
    }
}
