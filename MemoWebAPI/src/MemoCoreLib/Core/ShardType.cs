﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoCoreLib.Core
{
    // Shard content type.
    public enum ShardType
    {
        Unknown = 0,
        Text = 1,
        // Quote,
        // Link,
        // Conversation,
        Drawing = 11,    // user drawing. internal strucure.
        Image = 21,      // image file. bmp.
        Photo = 31,   // Image vs 
        Video = 41,
        Audio = 51,   // music, sound, voice recording, ...
        // ...

    }

    public static class ShardTypes
    {
        public static string ToName(this ShardType shardType)
        {
            try {
                return Enum.GetName(typeof(ShardType), shardType);
            } catch (Exception) {
                return null;
            }
        }
        public static ShardType ToShardType(this string value)
        {
            try {
                return (ShardType)Enum.Parse(typeof(ShardType), value);
            } catch (Exception) {
                return ShardType.Unknown;
            }
        }
    }

}
