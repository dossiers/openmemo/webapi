﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoCoreLib.Core
{
    // Predefined Board Size
    public enum SizeType
    {
        Unknown = 0,
        Custom = 1,    // ???

        // Square shapes only??
        TinyTinySquare = 100,
        TinySquare = 200,
        SmallSquare = 300,
        MediumSquare = 400,
        LargeSquare = 500,
        ExLargeSquare = 600,
        ExExLargeSquare = 700,
        ExExExLargeSquare = 800,
        ExExExExLargeSquare = 900,
        ExExExExExLargeSquare = 1000,

        // etc...

        // infinite size ???

    }

    public static class SizeTypes
    {
        public static string ToName(this SizeType sizeType)
        {
            try {
                return Enum.GetName(typeof(SizeType), sizeType);
            } catch (Exception) {
                return null;
            }
        }
        public static SizeType ToSizeType(this string value)
        {
            try {
                return (SizeType)Enum.Parse(typeof(SizeType), value);
            } catch (Exception) {
                return SizeType.Unknown;
            }
        }
    }

}
