﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoCoreLib.Core
{
    public enum AccessLevel
    {
        Unknown = 0,
        Public,
        Private,
        Shared,


    }

    public static class AccessLevels
    {
        public static string ToName(this AccessLevel accessLevel)
        {
            try {
                return Enum.GetName(typeof(AccessLevel), accessLevel);
            } catch(Exception) {
                return null;
            }
        }
        public static AccessLevel ToAccessLevel(this string value)
        {
            try {
                return (AccessLevel)Enum.Parse(typeof(AccessLevel), value);
            } catch(Exception) {
                return AccessLevel.Unknown;
            }
        }
    }
}
