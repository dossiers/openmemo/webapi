﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoCoreLib.Common
{
    // Location of a shard or a child board within a board
    public class ElementPosition
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static readonly ElementPosition Origin = new ElementPosition(0, 0);

        public ElementPosition(int x, int y)
            : this(x, y, BoardSize.Null)
        {
        }
        public ElementPosition(int x, int y, BoardSize parentSize)
        {
            X = x;
            Y = y;
            ParentSize = parentSize;
        }

        public int X { get; set; }
        public int Y { get; set; }
        public BoardSize ParentSize { get; }

        public override string ToString()
        {
            return $"({X},{Y})/{ParentSize}";
        }

    }
}
