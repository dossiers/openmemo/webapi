﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoCoreLib.Common
{
    public abstract class MemoElement
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(MemoElement source)
        {
            Id = source.Id;
            Created = source.Created;
            Updated = source.Updated;
            Deleted = source.Deleted;
        }

        public ulong Id { get; set; }

        public long Created { get; set; }
        public long Updated { get; set; }
        public long Deleted { get; set; }
    }
}
