﻿using HoloLC.MemoCoreLib.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoCoreLib.Models
{
    public class MemoBinder : MemoBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(MemoBinder source)
        {
            base.CopyDataFrom(source);
            HomeBoard = source.HomeBoard;
        }

        // The first MemoBoard.
        // This should be part of BinderChild records...
        //   ( --> How to enforce that? 
        //   ( Maybe, consider this as an extra board, if it is not present in the collection?
        //   ( If it is in the collection, just use it?
        // ...
        // The z-order of HomeBoard is infinite (regardless of the value defined in BinderChild.
        public ulong HomeBoard { get; set; }

        // ...
    }
}
