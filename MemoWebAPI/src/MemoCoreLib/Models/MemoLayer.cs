﻿using HoloLC.MemoCoreLib.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoCoreLib.Models
{
    public class MemoLayer : MemoBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(MemoLayer source)
        {
            base.CopyDataFrom(source);
            Board = source.Board;
            // Position = source.Position;
            ZOrder = ZOrder;
        }

        public ulong Board { get; set; }      // Layer belongs to a single parent MemoBoard.

        // Default layer for a given board???
        // == layer with the lowest zorder ????

        // Layer size is always the same as that of its parent board.
        // Hence, the position is not needed.
        // public ElementPosition Position { get; set; }

        public int ZOrder { get; set; }

        // ... 
    }
}
