﻿using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Core;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoCoreLib.Models
{
    public class MemoBoard : MemoBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(MemoBoard source)
        {
            base.CopyDataFrom(source);
            Label = source.Label;
            IsTerminal = source.IsTerminal;
        }

        // Primary "tag" (e.g., "work", "personal", etc.)
        //  --> one-many tagging????  --> TBD: Create separate "tag" and "board-tag" tables...
        // (what aobut binder????)
        public string Label { get; set; }
        // public IList<string> Labels { get; set; }
        // ....

        public bool IsTerminal { get; set; } = false;   // If true, it cannot have child boards.
        // Used when it is included in another (parent) board???
        // Just use Title???
        // public string Synopsis { get; set; }

        // tbd:
        // how to specify parent board(1) - child board(n) relationship ???
        // child board location, z-order,  overrideable access level, ....
        // ... 
    }
}
