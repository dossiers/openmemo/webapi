﻿using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Core;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoCoreLib.Models
{
    public class MemoShard : MemoElement
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // Data copy
        public void CopyDataFrom(MemoShard source)
        {
            base.CopyDataFrom(source);
            Board = source.Board;
            Layer = source.Layer;
            Type = source.Type;
            Position = source.Position;
            ZOrder = ZOrder;
        }

        public ulong Board { get; set; }      // Shard belongs to a single parent MemoBoard.  Board == Layer.Board.
        public ulong Layer { get; set; }      // Shard belongs to a single parent MemoLayer.
        // public ulong Owner { get; set; }   // owner information inherits from the Board.Owner.
        public ShardType Type { get; set; }
        // size ???
        public ElementPosition Position { get; set; }
        public int ZOrder { get; set; }
        // ...

        // tbd:
        // how to store the actual content?
        // ....

    }
}
