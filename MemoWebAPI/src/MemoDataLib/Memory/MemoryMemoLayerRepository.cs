﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System.Collections.Concurrent;
using NLog;

namespace HoloLC.MemoDataLib.Memory
{
    public class MemoryMemoLayerRepository : IMemoLayerRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // ???
        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, MemoLayer>> layerMap = new ConcurrentDictionary<ulong, IDictionary<ulong, MemoLayer>>();

        // tbd:

        public async Task<MemoLayer> GetItemByOwnerAsync(ulong id, ulong owner = 0)
        {
            if (owner > 0UL) {
                IDictionary<ulong, MemoLayer> map;
                if (layerMap.TryGetValue(owner, out map)) {
                    if (map.ContainsKey(id)) {
                        var memoLayer = map[owner];
                        return memoLayer;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }
        public async Task<MemoLayer> GetItemByBoardAsync(ulong id, ulong board = 0)
        {
            if (board > 0UL) {
                IDictionary<ulong, MemoLayer> map;
                if (layerMap.TryGetValue(board, out map)) {
                    if (map.ContainsKey(id)) {
                        var memoLayer = map[board];
                        return memoLayer;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<IEnumerable<MemoLayer>> FindAllByOwnerAsync(ulong owner)
        {
            IDictionary<ulong, MemoLayer> map;
            if (layerMap.TryGetValue(owner, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }
        public async Task<IEnumerable<MemoLayer>> FindAllByBoardAsync(ulong board)
        {
            IDictionary<ulong, MemoLayer> map;
            if (layerMap.TryGetValue(board, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }

        //public async Task<IEnumerable<MemoLayer> FindByType(ulong board, LayerType type)
        //{
        //    throw new NotImplementedException();
        //}


        public async Task<bool> AddItemAsync(MemoLayer layer)
        {
            var id = layer.Id;
            var board = layer.Owner;
            IDictionary<ulong, MemoLayer> map;
            if (layerMap.TryGetValue(board, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the id across different boards....
                if (map.ContainsKey(id)) {
                    System.Diagnostics.Debug.WriteLine($"MemoLayer cannot be added because an item with the given id already exists: id = {id}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, MemoLayer>();
                layerMap[board] = map;
            }

            // TBD: Update the Created/Updated field?
            map[id] = layer;
            return true;
        }

        public async Task<bool> ReplaceItemAsync(MemoLayer memoLayer, bool createIfNotExist = false)
        {
            var id = memoLayer.Id;
            var board = memoLayer.Owner;
            IDictionary<ulong, MemoLayer> map;
            if (layerMap.TryGetValue(board, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, MemoLayer>();
                    layerMap[board] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[id] = memoLayer;
            return true;
        }


        public async Task<MemoLayer> DeleteItemByOwnerAsync(ulong id, ulong owner = 0)
        {
            if (owner > 0UL) {
                IDictionary<ulong, MemoLayer> map;
                if (layerMap.TryGetValue(owner, out map)) {
                    var memoLayer = map[id];
                    if (map.Remove(id)) {
                        return memoLayer;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }
        public async Task<MemoLayer> DeleteItemByBoardAsync(ulong id, ulong board = 0)
        {
            if (board > 0UL) {
                IDictionary<ulong, MemoLayer> map;
                if (layerMap.TryGetValue(board, out map)) {
                    var memoLayer = map[id];
                    if (map.Remove(id)) {
                        return memoLayer;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            IDictionary<ulong, MemoLayer> map;
            if (layerMap.TryRemove(owner, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }
        public async Task<int> RemoveAllByBoardAsync(ulong board)
        {
            IDictionary<ulong, MemoLayer> map;
            if (layerMap.TryRemove(board, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

        //public int RemoveByType(ulong board, LayerType type)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
