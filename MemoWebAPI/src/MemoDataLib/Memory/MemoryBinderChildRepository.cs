﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using System.Collections.Concurrent;
using NLog;

namespace HoloLC.MemoDataLib.Memory
{
    public class MemoryBinderChildRepository : IBinderChildRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, BinderChild>> binderChildMap = new ConcurrentDictionary<ulong, IDictionary<ulong, BinderChild>>();

        public async Task<IEnumerable<BinderChild>> FindMemoBoardsAsync(ulong binderId)
        {
            IDictionary<ulong, BinderChild> map;
            if (binderChildMap.TryGetValue(binderId, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }

        public async Task<bool> AddBinderChildAsync(BinderChild binderChild)
        {
            var binderId = binderChild.BinderId;
            var boardId = binderChild.BoardId;
            IDictionary<ulong, BinderChild> map;
            if (binderChildMap.TryGetValue(binderId, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the boardId across different binderIds....
                if (map.ContainsKey(boardId)) {
                    System.Diagnostics.Debug.WriteLine($"BinderChild cannot be added because an item with the given boardId already exists: boardId = {boardId}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, BinderChild>();
                binderChildMap[binderId] = map;
            }

            // TBD: Update the Created/Updated field?
            map[boardId] = binderChild;
            return true;
        }

        public async Task<bool> ReplaceBinderChildAsync(BinderChild binderChild, bool createIfNotExist = false)
        {
            var binderId = binderChild.BinderId;
            var boardId = binderChild.BoardId;
            IDictionary<ulong, BinderChild> map;
            if (binderChildMap.TryGetValue(binderId, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, BinderChild>();
                    binderChildMap[binderId] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[boardId] = binderChild;
            return true;
        }

        public async Task<BinderChild> DeleteBinderChildAsync(ulong binderId, ulong boardId)
        {
            IDictionary<ulong, BinderChild> map;
            if (binderChildMap.TryGetValue(binderId, out map)) {
                var binderChild = map[boardId];
                if (map.Remove(boardId)) {
                    return binderChild;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        public async Task<int> RemoveByBinderAsync(ulong binderId)
        {
            IDictionary<ulong, BinderChild> map;
            if (binderChildMap.TryRemove(binderId, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

    }
}
