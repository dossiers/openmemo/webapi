﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System.Collections.Concurrent;
using NLog;

namespace HoloLC.MemoDataLib.Memory
{
    public class MemoryMemoBoardRepository : IMemoBoardRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, MemoBoard>> memoBoardMap = new ConcurrentDictionary<ulong, IDictionary<ulong, MemoBoard>>();


        public async Task<MemoBoard> GetItemAsync(ulong id, ulong owner = 0UL)
        {
            if (owner > 0UL) {
                IDictionary<ulong, MemoBoard> map;
                if (memoBoardMap.TryGetValue(owner, out map)) {
                    if (map.ContainsKey(id)) {
                        var memoBoard = map[owner];
                        return memoBoard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<IEnumerable<MemoBoard>> FindAllByOwnerAsync(ulong owner)
        {
            IDictionary<ulong, MemoBoard> map;
            if (memoBoardMap.TryGetValue(owner, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }


        public async Task<bool> AddItemAsync(MemoBoard memoBoard)
        {
            var id = memoBoard.Id;
            var owner = memoBoard.Owner;
            IDictionary<ulong, MemoBoard> map;
            if (memoBoardMap.TryGetValue(owner, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the id across different owners....
                if (map.ContainsKey(id)) {
                    System.Diagnostics.Debug.WriteLine($"MemoBoard cannot be added because an item with the given id already exists: id = {id}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, MemoBoard>();
                memoBoardMap[owner] = map;
            }

            // TBD: Update the Created/Updated field?
            map[id] = memoBoard;
            return true;
        }

        public async Task<bool> ReplaceItemAsync(MemoBoard memoBoard, bool createIfNotExist = false)
        {
            var id = memoBoard.Id;
            var owner = memoBoard.Owner;
            IDictionary<ulong, MemoBoard> map;
            if (memoBoardMap.TryGetValue(owner, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, MemoBoard>();
                    memoBoardMap[owner] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[id] = memoBoard;
            return true;
        }


        public async Task<MemoBoard> DeleteItemAsync(ulong id, ulong owner = 0UL)
        {
            if (owner > 0UL) {
                IDictionary<ulong, MemoBoard> map;
                if (memoBoardMap.TryGetValue(owner, out map)) {
                    var memoBoard = map[id];
                    if (map.Remove(id)) {
                        return memoBoard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            IDictionary<ulong, MemoBoard> map;
            if (memoBoardMap.TryRemove(owner, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

        //public int RemoveByType(ulong owner, ShardType type)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
