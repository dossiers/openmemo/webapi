﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System.Collections.Concurrent;
using NLog;

namespace HoloLC.MemoDataLib.Memory
{
    public class MemoryMemoShardRepository : IMemoShardRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // ???
        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, MemoShard>> shardMap = new ConcurrentDictionary<ulong, IDictionary<ulong, MemoShard>>();

        // tbd:

        public async Task<MemoShard> GetItemByLayerAsync(ulong id, ulong layer = 0)
        {
            if (layer > 0UL) {
                IDictionary<ulong, MemoShard> map;
                if (shardMap.TryGetValue(layer, out map)) {
                    if (map.ContainsKey(id)) {
                        var memoShard = map[layer];
                        return memoShard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }
        public async Task<MemoShard> GetItemByBoardAsync(ulong id, ulong board = 0)
        {
            if(board > 0UL) {
                IDictionary<ulong, MemoShard> map;
                if (shardMap.TryGetValue(board, out map)) {
                    if (map.ContainsKey(id)) {
                        var memoShard = map[board];
                        return memoShard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<IEnumerable<MemoShard>> FindAllByLayerAsync(ulong layer)
        {
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryGetValue(layer, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }
        public async Task<IEnumerable<MemoShard>> FindAllByBoardAsync(ulong board)
        {
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryGetValue(board, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }

        //public IEnumerable<MemoShard> FindByType(ulong board, ShardType type)
        //{
        //    throw new NotImplementedException();
        //}


        public async Task<bool> AddItemAsync(MemoShard shard)
        {
            var id = shard.Id;
            var board = shard.Layer;
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryGetValue(board, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the id across different boards....
                if (map.ContainsKey(id)) {
                    System.Diagnostics.Debug.WriteLine($"MemoShard cannot be added because an item with the given id already exists: id = {id}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, MemoShard>();
                shardMap[board] = map;
            }

            // TBD: Update the Created/Updated field?
            map[id] = shard;
            return true;
        }

        public async Task<bool> ReplaceItemAsync(MemoShard memoShard, bool createIfNotExist = false)
        {
            var id = memoShard.Id;
            var board = memoShard.Layer;
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryGetValue(board, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, MemoShard>();
                    shardMap[board] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[id] = memoShard;
            return true;
        }


        public async Task<MemoShard> DeleteItemByLayerAsync(ulong id, ulong layer = 0)
        {
            if (layer > 0UL) {
                IDictionary<ulong, MemoShard> map;
                if (shardMap.TryGetValue(layer, out map)) {
                    var memoShard = map[id];
                    if (map.Remove(id)) {
                        return memoShard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }
        public async Task<MemoShard> DeleteItemByBoardAsync(ulong id, ulong board = 0)
        {
            if (board > 0UL) {
                IDictionary<ulong, MemoShard> map;
                if (shardMap.TryGetValue(board, out map)) {
                    var memoShard = map[id];
                    if (map.Remove(id)) {
                        return memoShard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public async Task<int> RemoveAllByLayerAsync(ulong layer)
        {
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryRemove(layer, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }
        public async Task<int> RemoveAllByBoardAsync(ulong board)
        {
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryRemove(board, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

        //public int RemoveByType(ulong board, ShardType type)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
