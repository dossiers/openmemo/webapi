﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoDataLib.Repositories;
using HoloLC.MemoDataLib.Dynamo;
using NLog;
using HoloLC.MemoDataLib.Dynamo.Tables;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories;

namespace HoloLC.MemoDataLib.Managers.Impl
{
    public sealed class DynamoRepositoryManager : IInstantRepositoryManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IBinderChildRepository binderChildRepository;
        private readonly IBoardChildRepository boardChildRepository;
        private readonly IMemoBinderRepository memoBinderRepository;
        private readonly IMemoBoardRepository memoBoardRepository;
        private readonly IMemoLayerRepository memoLayerRepository;
        private readonly IMemoShardRepository memoShardRepository;

        public DynamoRepositoryManager(ITableCollection tableCollection)
        {
            binderChildRepository = new DynamoBinderChildRepository(
                tableCollection.GetTable(DynamoBinderChildTable.TABLE_KEY)
                ?? new DynamoBinderChildTable(tableCollection.ClientContext));
            boardChildRepository = new DynamoBoardChildRepository(
                tableCollection.GetTable(DynamoBoardChildTable.TABLE_KEY)
                ?? new DynamoBoardChildTable(tableCollection.ClientContext));
            memoBinderRepository = new DynamoMemoBinderRepository(
                tableCollection.GetTable(DynamoMemoBinderTable.TABLE_KEY)
                ?? new DynamoMemoBinderTable(tableCollection.ClientContext));
            memoBoardRepository = new DynamoMemoBoardRepository(
                tableCollection.GetTable(DynamoMemoBoardTable.TABLE_KEY)
                ?? new DynamoMemoBoardTable(tableCollection.ClientContext));
            memoLayerRepository = new DynamoMemoLayerRepository(
                tableCollection.GetTable(DynamoMemoLayerTable.TABLE_KEY)
                ?? new DynamoMemoLayerTable(tableCollection.ClientContext));
            memoShardRepository = new DynamoMemoShardRepository(
                tableCollection.GetTable(DynamoMemoShardTable.TABLE_KEY)
                ?? new DynamoMemoShardTable(tableCollection.ClientContext));
        }


        public IBinderChildRepository BinderChildRepository
        {
            get
            {
                return binderChildRepository;
            }
        }

        public IBoardChildRepository BoardChildRepository
        {
            get
            {
                return boardChildRepository;
            }
        }

        public IMemoBinderRepository MemoBinderRepository
        {
            get
            {
                return memoBinderRepository;
            }
        }

        public IMemoBoardRepository MemoBoardRepository
        {
            get
            {
                return memoBoardRepository;
            }
        }

        public IMemoLayerRepository MemoLayerRepository
        {
            get
            {
                return memoLayerRepository;
            }
        }

        public IMemoShardRepository MemoShardRepository
        {
            get
            {
                return memoShardRepository;
            }
        }


        public async Task CreateAllAsync(bool recreateIfPresent = false)
        {
            await (BinderChildRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (BoardChildRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (MemoBinderRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (MemoBoardRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (MemoLayerRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
            await (MemoShardRepository as IInstantRepository)?.CreateTableAsync(recreateIfPresent);
        }

        public async Task DeleteAllAsync()
        {
            await (BinderChildRepository as IInstantRepository)?.DeleteTableAsync();
            await (BoardChildRepository as IInstantRepository)?.DeleteTableAsync();
            await (MemoBinderRepository as IInstantRepository)?.DeleteTableAsync();
            await (MemoBoardRepository as IInstantRepository)?.DeleteTableAsync();
            await (MemoLayerRepository as IInstantRepository)?.DeleteTableAsync();
            await (MemoShardRepository as IInstantRepository)?.DeleteTableAsync();
        }

        public async Task PurgeAllAsync(bool createIfAbsent = false)
        {
            await (BinderChildRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (BoardChildRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (MemoBinderRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (MemoBoardRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (MemoLayerRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
            await (MemoShardRepository as IInstantRepository)?.PurgeTableAsync(createIfAbsent);
        }

    }
}
