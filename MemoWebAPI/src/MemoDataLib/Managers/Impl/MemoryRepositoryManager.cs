﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoDataLib.Repositories;
using HoloLC.MemoDataLib.Memory;
using NLog;

namespace HoloLC.MemoDataLib.Managers.Impl
{
    public sealed class MemoryRepositoryManager : IDataRepositoryManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MemoryRepositoryManager()
        {
        }

        private IBinderChildRepository binderChildRepository = new MemoryBinderChildRepository();
        private IBoardChildRepository boardChildRepository = new MemoryBoardChildRepository();
        private IMemoBinderRepository memoBinderRepository = new MemoryMemoBinderRepository();
        private IMemoBoardRepository memoBoardRepository = new MemoryMemoBoardRepository();
        private IMemoLayerRepository memoLayerRepository = new MemoryMemoLayerRepository();
        private IMemoShardRepository memoShardRepository = new MemoryMemoShardRepository();


        public IBinderChildRepository BinderChildRepository
        {
            get
            {
                return binderChildRepository;
            }
        }

        public IBoardChildRepository BoardChildRepository
        {
            get
            {
                return boardChildRepository;
            }
        }

        public IMemoBinderRepository MemoBinderRepository
        {
            get
            {
                return memoBinderRepository;
            }
        }

        public IMemoBoardRepository MemoBoardRepository
        {
            get
            {
                return memoBoardRepository;
            }
        }

        public IMemoLayerRepository MemoLayerRepository
        {
            get
            {
                return memoLayerRepository;
            }
        }

        public IMemoShardRepository MemoShardRepository
        {
            get
            {
                return memoShardRepository;
            }
        }
    }
}
