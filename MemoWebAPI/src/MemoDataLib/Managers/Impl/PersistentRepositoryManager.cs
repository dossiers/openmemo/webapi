﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoDataLib.Repositories;
using HoloLC.MemoDataLib.Persistent;
using NLog;
using HoloLC.MemoDataLib.DB;

namespace HoloLC.MemoDataLib.Managers.Impl
{
    public sealed class PersistentRepositoryManager : IDataRepositoryManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IBinderChildRepository binderChildRepository;
        private readonly IBoardChildRepository boardChildRepository;
        private readonly IMemoBinderRepository memoBinderRepository;
        private readonly IMemoBoardRepository memoBoardRepository;
        private readonly IMemoLayerRepository memoLayerRepository;
        private readonly IMemoShardRepository memoShardRepository;

        public PersistentRepositoryManager(MemoDbContext dbContext)
        {
            binderChildRepository = new PersistentBinderChildRepository(dbContext);
            boardChildRepository = new PersistentBoardChildRepository(dbContext);
            memoBinderRepository = new PersistentMemoBinderRepository(dbContext);
            memoBoardRepository = new PersistentMemoBoardRepository(dbContext);
            memoLayerRepository = new PersistentMemoLayerRepository(dbContext);
            memoShardRepository = new PersistentMemoShardRepository(dbContext);
        }


        public IBinderChildRepository BinderChildRepository
        {
            get
            {
                return binderChildRepository;
            }
        }

        public IBoardChildRepository BoardChildRepository
        {
            get
            {
                return boardChildRepository;
            }
        }

        public IMemoBinderRepository MemoBinderRepository
        {
            get
            {
                return memoBinderRepository;
            }
        }

        public IMemoBoardRepository MemoBoardRepository
        {
            get
            {
                return memoBoardRepository;
            }
        }

        public IMemoLayerRepository MemoLayerRepository
        {
            get
            {
                return memoLayerRepository;
            }
        }

        public IMemoShardRepository MemoShardRepository
        {
            get
            {
                return memoShardRepository;
            }
        }
    }
}
