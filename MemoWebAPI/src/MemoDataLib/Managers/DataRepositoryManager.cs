﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Managers
{
    public interface IDataRepositoryManager
    {
        IMemoBinderRepository MemoBinderRepository { get; }
        IBinderChildRepository BinderChildRepository { get; }
        IMemoBoardRepository MemoBoardRepository { get; }
        IBoardChildRepository BoardChildRepository { get; }
        IMemoLayerRepository MemoLayerRepository { get; }
        IMemoShardRepository MemoShardRepository { get; }

    }
}
