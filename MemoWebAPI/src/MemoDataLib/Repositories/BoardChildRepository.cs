﻿using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace HoloLC.MemoDataLib.Repositories
{
    public interface IBoardChildRepository
    {
        Task<IEnumerable<BoardChild>> FindChildrenAsync(ulong parent);
        // IEnumerable<BoardChild> FindParents(ulong child);

        Task<bool> AddBoardChildAsync(BoardChild boardChild);
        // BoardChild CreateBoardChild(ulong parentId, ulong childId, ElementPosition ChildPosition, int zOrder = 0);

        Task<bool> ReplaceBoardChildAsync(BoardChild boardChild, bool createIfNotExist = false);
        // BoardChild UpdateChildPosition(ulong parentId, ulong childId, ElementPosition ChildPosition);
        // BoardChild UpdateChildZOrder(ulong parentId, ulong childId, int zOrder);

        Task<BoardChild> DeleteBoardChildAsync(ulong parentId, ulong childId);
        Task<int> RemoveByParentAsync(ulong parentId);
        // int RemoveByChild(ulong childId);

    }
}
