﻿using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Repositories
{
    public interface IMemoBinderRepository
    {
        Task<MemoBinder> GetItemAsync(ulong id, ulong owner = 0UL);
        // IEnumerable<MemoBinder> FindAll();
        Task<IEnumerable<MemoBinder>> FindAllByOwnerAsync(ulong owner);
        // IEnumerable<MemoBinder> FindByAccessLevel(AccessLevel level);
        // IEnumerable<MemoBinder> FindByOwnerAndAccessLevel(ulong owner, AccessLevel level);

        // How/where to store actual content ???
        Task<bool> AddItemAsync(MemoBinder memoBinder);
        // MemoBinder CreateItem(ulong owner, AccessLevel level, BoardSize size, string title = null, string description = null, bool isTerminal = false);

        // "Overwrite" semantics If the item does not exist, create one. ?? Or, ignore and return null ??
        Task<bool> ReplaceItemAsync(MemoBinder memoBinder, bool createIfNotExist = false);
        // MemoBinder UpdateAccessLevel(ulong id, AccessLevel level);
        // MemoBinder UpdateTitle(ulong id, string title);

        Task<MemoBinder> DeleteItemAsync(ulong id, ulong owner = 0UL);
        Task<int> RemoveAllByOwnerAsync(ulong owner);
        // int RemoveByOwnerAndAccessLevel(ulong owner, AccessLevel level);
    }
}
