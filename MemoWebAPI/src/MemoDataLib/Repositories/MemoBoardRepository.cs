﻿using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Repositories
{
    public interface IMemoBoardRepository
    {
        Task<MemoBoard> GetItemAsync(ulong id, ulong owner = 0UL);
        // IEnumerable<MemoBoard> FindAll();
        Task<IEnumerable<MemoBoard>> FindAllByOwnerAsync(ulong owner);
        // IEnumerable<MemoBoard> FindByAccessLevel(AccessLevel level);
        // IEnumerable<MemoBoard> FindByOwnerAndAccessLevel(ulong owner, AccessLevel level);

        // How/where to store actual content ???
        Task<bool> AddItemAsync(MemoBoard board);
        // MemoBoard CreateItem(ulong owner, AccessLevel level, BoardSize size, string title = null, string description = null, bool isTerminal = false);

        // "Overwrite" semantics If the item does not exist, create one. ?? Or, ignore and return null ??
        Task<bool> ReplaceItemAsync(MemoBoard memoBoard, bool createIfNotExist = false);
        // MemoBoard UpdateAccessLevel(ulong id, AccessLevel level);
        // MemoBoard UpdateTitle(ulong id, string title);

        Task<MemoBoard> DeleteItemAsync(ulong id, ulong owner = 0UL);
        Task<int> RemoveAllByOwnerAsync(ulong owner);
        // int RemoveByOwnerAndAccessLevel(ulong owner, AccessLevel level);

    }
}
