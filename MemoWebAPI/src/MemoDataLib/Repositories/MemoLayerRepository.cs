﻿using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Repositories
{
    public interface IMemoLayerRepository
    {
        Task<MemoLayer> GetItemByOwnerAsync(ulong id, ulong owner = 0UL);
        Task<MemoLayer> GetItemByBoardAsync(ulong id, ulong board = 0UL);
        Task<IEnumerable<MemoLayer>> FindAllByOwnerAsync(ulong owner);
        Task<IEnumerable<MemoLayer>> FindAllByBoardAsync(ulong board);

        Task<bool> AddItemAsync(MemoLayer layer);

        Task<bool> ReplaceItemAsync(MemoLayer memoLayer, bool createIfNotExist = false);

        Task<MemoLayer> DeleteItemByOwnerAsync(ulong id, ulong owner = 0UL);
        Task<MemoLayer> DeleteItemByBoardAsync(ulong id, ulong board = 0UL);
        Task<int> RemoveAllByOwnerAsync(ulong owner);
        Task<int> RemoveAllByBoardAsync(ulong board);

    }
}
