﻿using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Repositories
{
    public interface IBinderChildRepository
    {
        Task<IEnumerable<BinderChild>> FindMemoBoardsAsync(ulong binderId);
        // IEnumerable<BinderChild> FindMemoBinders(ulong boardId);

        Task<bool> AddBinderChildAsync(BinderChild binderChild);
        // BinderChild CreateBinderChild(ulong binderId, ulong boardId, int zOrder = 0);

        Task<bool> ReplaceBinderChildAsync(BinderChild binderChild, bool createIfNotExist = false);
        // BinderChild UpdateChildPosition(ulong binderId, ulong boardId);
        // BinderChild UpdateChildZOrder(ulong binderId, ulong boardId, int zOrder);

        Task<BinderChild> DeleteBinderChildAsync(ulong binderId, ulong boardId);
        Task<int> RemoveByBinderAsync(ulong binderId);
        // int RemoveByBoard(ulong boardId);

    }
}
