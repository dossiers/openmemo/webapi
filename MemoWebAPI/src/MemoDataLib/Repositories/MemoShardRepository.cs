﻿using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Repositories
{
    public interface IMemoShardRepository
    {
        Task<MemoShard> GetItemByLayerAsync(ulong id, ulong layer = 0UL);
        Task<MemoShard> GetItemByBoardAsync(ulong id, ulong board = 0UL);
        Task<IEnumerable<MemoShard>> FindAllByLayerAsync(ulong layer);
        Task<IEnumerable<MemoShard>> FindAllByBoardAsync(ulong board);
        // IEnumerable<MemoShard> FindByType(ulong board, ShardType type);
        // IEnumerable<MemoShard> FindByTypes(ulong board, ShardType[] types);

        // How/where to store actual content ???
        Task<bool> AddItemAsync(MemoShard shard);
        // MemoShard CreateItem(ulong board, ShardType type, ElementPosition position, int zOrder = 0);

        // "Overwrite" syntax. If the item does not exist, create one. ?? Or, ignore and return null ??
        Task<bool> ReplaceItemAsync(MemoShard memoShard, bool createIfNotExist = false);
        // MemoShard UpdatePosition(ulong id, ElementPosition position);
        // MemoShard UpdateZOrder(ulong id, int zOrder);

        Task<MemoShard> DeleteItemByLayerAsync(ulong id, ulong layer = 0UL);
        Task<MemoShard> DeleteItemByBoardAsync(ulong id, ulong board = 0UL);
        Task<int> RemoveAllByLayerAsync(ulong layer);
        Task<int> RemoveAllByBoardAsync(ulong board);
        // int RemoveByType(ulong board, ShardType type);
        // int RemoveByTypes(ulong board, ShardType[] types);

    }
}
