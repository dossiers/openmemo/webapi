﻿using HoloLC.MemoCoreLib.Common;
using HoloLC.MemoCoreLib.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.DB
{
    public class MemoDbContext : DbContext
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MemoDbContext(DbContextOptions<MemoDbContext> options)
            : base(options)
        {
        }

        public DbSet<MemoBinder> MemoBinders { get; set; }
        public DbSet<MemoBoard> MemoBoards { get; set; }
        public DbSet<MemoLayer> MemoLayers { get; set; }
        public DbSet<MemoShard> MemoShards { get; set; }
        public DbSet<BinderChild> BinderChildren { get; set; }
        public DbSet<BoardChild> BoardChildren { get; set; }

        // ????
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Set this in the web api module???
            // optionsBuilder.UseSqlite("connection string");
            // ....
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<MemoBinder>().HasKey(m => m.Id);
            builder.Entity<MemoBoard>().HasKey(m => m.Id);
            builder.Entity<MemoLayer>().HasKey(m => m.Id);
            builder.Entity<MemoShard>().HasKey(m => m.Id);
            builder.Entity<BinderChild>().HasKey(m => new { m.BinderId, m.BoardId });
            builder.Entity<BoardChild>().HasKey(m => new { m.ParentId, m.ChildId });
            builder.Entity<ElementPosition>().HasKey(m => new { m.X, m.Y });

            // Exclude "derived" fields...
            builder.Entity<MemoBinder>().Ignore(nameof(MemoBinder.Size));
            builder.Entity<MemoBoard>().Ignore(nameof(MemoBoard.Size));
            builder.Entity<MemoLayer>().Ignore(nameof(MemoLayer.Size));

            base.OnModelCreating(builder);
        }

    }
}
