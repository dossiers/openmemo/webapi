﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using HoloLC.MemoDataLib.DB;

namespace HoloLC.MemoDataLib.Persistent
{
    public class PersistentMemoBinderRepository : IMemoBinderRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly MemoDbContext dbContext;

        public PersistentMemoBinderRepository(MemoDbContext dbContext)
        {
            this.dbContext = dbContext;
        }



        public async Task<IEnumerable<MemoBinder>> FindAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"FindAll() owner = {owner}.");

            var queryable = dbContext.MemoBinders
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAll() returing null. owner = {owner}.");
            return null;
        }

        public async Task<MemoBinder> GetItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"GetItem() id = {id}; owner = {owner}.");

            var memoBinder = dbContext.MemoBinders.FirstOrDefault(o => o.Id == id && o.Owner == owner);
            return memoBinder;
        }

        public async Task<bool> AddItemAsync(MemoBinder memoBinder)
        {
            Logger.Trace($"AddItem() pmemoBinder = {memoBinder}.");

            dbContext.MemoBinders.Add(memoBinder);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoShard saved: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                Logger.Debug($"MermoShard saved: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoShard: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                Logger.Warn($"Failed to save MermoShard: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                return false;
            }
        }

        public async Task<bool> ReplaceItemAsync(MemoBinder memoBinder, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() pmemoBinder = {memoBinder}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingMemoBinder = dbContext.MemoBinders.FirstOrDefault(o => o.Owner == memoBinder.Owner && o.Id == memoBinder.Id);
            if (existingMemoBinder != null) {
                existingMemoBinder.CopyDataFrom(memoBinder);
                dbContext.MemoBinders.Update(existingMemoBinder);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard replaced: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                    Logger.Debug($"MermoShard replaced: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoShard: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                    Logger.Warn($"Failed to replace MermoShard: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.MemoBinders.Add(memoBinder);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                        Logger.Debug($"New pickupbox added: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoShard: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                        Logger.Warn($"Failed to add new MermoShard: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoShard not found: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                    Logger.Warn($"MermoShard not found: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                }
            }
            return (saved > 0);
        }


        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"RemoveAll() owner = {owner}.");

            var queryable = dbContext.MemoBinders
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.MemoBinders.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. owner = {owner}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. owner = {owner}.");
                return saved;
            }
            return 0;
        }

        public async Task<MemoBinder> DeleteItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"DeleteById() owner = {owner}; id = {id}.");

            var memoBinder = dbContext.MemoBinders.FirstOrDefault(o => o.Owner == owner && o.Id == id);
            if (memoBinder != null) {
                dbContext.MemoBinders.Remove(memoBinder);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                    Logger.Debug($"MermoShard deleted: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                    return memoBinder;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                    Logger.Warn($"Failed to deleted pickupbox: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                    return null;
                }
            }
            return null;
        }
    }
}
