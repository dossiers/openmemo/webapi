﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using HoloLC.MemoDataLib.DB;

namespace HoloLC.MemoDataLib.Persistent
{
    public class PersistentBinderChildRepository : IBinderChildRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly MemoDbContext dbContext;

        public PersistentBinderChildRepository(MemoDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public async Task<IEnumerable<BinderChild>> FindMemoBoardsAsync(ulong binderId)
        {
            Logger.Trace($"FindMemoBoards() binderId = {binderId}.");

            var queryable = dbContext.BinderChildren
                .Select(o => o)
                .Where(o => o.BinderId == binderId);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAll() returing null. binderId = {binderId}.");
            return null;
        }

        public async Task<bool> AddBinderChildAsync(BinderChild binderChild)
        {
            Logger.Trace($"AddBinderChild() binderChild = {binderChild}.");

            dbContext.BinderChildren.Add(binderChild);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoShard saved: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                Logger.Debug($"MermoShard saved: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoShard: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                Logger.Warn($"Failed to save MermoShard: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                return false;
            }
        }

        public async Task<bool> ReplaceBinderChildAsync(BinderChild binderChild, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() binderChild = {binderChild}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingBinderChild = dbContext.BinderChildren.FirstOrDefault(o => o.BinderId == binderChild.BinderId && o.BoardId == binderChild.BoardId);
            if (existingBinderChild != null) {
                existingBinderChild.CopyDataFrom(binderChild);
                dbContext.BinderChildren.Update(existingBinderChild);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard replaced: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                    Logger.Debug($"MermoShard replaced: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoShard: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                    Logger.Warn($"Failed to replace MermoShard: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.BinderChildren.Add(binderChild);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                        Logger.Debug($"New pickupbox added: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoShard: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                        Logger.Warn($"Failed to add new MermoShard: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoShard not found: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                    Logger.Warn($"MermoShard not found: id = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                }
            }
            return (saved > 0);
        }

        public async Task<int> RemoveByBinderAsync(ulong binderId)
        {
            Logger.Trace($"RemoveByBinder() binderId = {binderId}.");

            var queryable = dbContext.BinderChildren
                .Select(o => o)
                .Where(o => o.BinderId == binderId);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.BinderChildren.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. binderId = {binderId}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. binderId = {binderId}.");
                return saved;
            }
            return 0;
        }

        public async Task<BinderChild> DeleteBinderChildAsync(ulong binderId, ulong boardId)
        {
            Logger.Trace($"DeleteById() binderId = {binderId}; boardId = {boardId}.");

            var binderChild = dbContext.BinderChildren.FirstOrDefault(o => o.BinderId == binderId && o.BoardId == boardId);
            if (binderChild != null) {
                dbContext.BinderChildren.Remove(binderChild);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: boardId = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                    Logger.Debug($"MermoShard deleted: boardId = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                    return binderChild;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: boardId = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                    Logger.Warn($"Failed to deleted pickupbox: boardId = {binderChild.BoardId}. binderId = {binderChild.BinderId}.");
                    return null;
                }
            }
            return null;
        }

    }
}
