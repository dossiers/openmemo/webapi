﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using HoloLC.MemoDataLib.DB;

namespace HoloLC.MemoDataLib.Persistent
{
    public class PersistentMemoShardRepository : IMemoShardRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly MemoDbContext dbContext;

        public PersistentMemoShardRepository(MemoDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public async Task<IEnumerable<MemoShard>> FindAllByLayerAsync(ulong layer)
        {
            Logger.Trace($"FindAllByLayer() layer = {layer}.");

            var queryable = dbContext.MemoShards
                .Select(o => o)
                .Where(o => o.Layer == layer);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAllByLayer() returing null. layer = {layer}.");
            return null;
        }
        public async Task<IEnumerable<MemoShard>> FindAllByBoardAsync(ulong board)
        {
            Logger.Trace($"FindAllByBoard() board = {board}.");

            var queryable = dbContext.MemoShards
                .Select(o => o)
                .Where(o => o.Layer == board);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAllByBoard() returing null. board = {board}.");
            return null;
        }

        public async Task<MemoShard> GetItemByLayerAsync(ulong id, ulong layer = 0)
        {
            Logger.Trace($"GetItemByLayer() id = {id}; layer = {layer}.");

            var memoShard = dbContext.MemoShards.FirstOrDefault(o => o.Id == id && o.Layer == layer);
            return memoShard;
        }
        public async Task<MemoShard> GetItemByBoardAsync(ulong id, ulong board = 0)
        {
            Logger.Trace($"GetItemByBoard() id = {id}; board = {board}.");

            var memoShard = dbContext.MemoShards.FirstOrDefault(o => o.Id == id && o.Layer == board);
            return memoShard;
        }

        public async Task<bool> AddItemAsync(MemoShard memoShard)
        {
            Logger.Trace($"AddItem() pmemoShard = {memoShard}.");

            dbContext.MemoShards.Add(memoShard);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoShard saved: id = {memoShard.Id}. board = {memoShard.Layer}.");
                Logger.Debug($"MermoShard saved: id = {memoShard.Id}. board = {memoShard.Layer}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoShard: id = {memoShard.Id}. board = {memoShard.Layer}.");
                Logger.Warn($"Failed to save MermoShard: id = {memoShard.Id}. board = {memoShard.Layer}.");
                return false;
            }
        }

        public async Task<bool> ReplaceItemAsync(MemoShard memoShard, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() pmemoShard = {memoShard}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingMemoShard = dbContext.MemoShards.FirstOrDefault(o => o.Layer == memoShard.Layer && o.Id == memoShard.Id);
            if (existingMemoShard != null) {
                existingMemoShard.CopyDataFrom(memoShard);
                dbContext.MemoShards.Update(existingMemoShard);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard replaced: id = {memoShard.Id}. board = {memoShard.Layer}.");
                    Logger.Debug($"MermoShard replaced: id = {memoShard.Id}. board = {memoShard.Layer}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoShard: id = {memoShard.Id}. board = {memoShard.Layer}.");
                    Logger.Warn($"Failed to replace MermoShard: id = {memoShard.Id}. board = {memoShard.Layer}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.MemoShards.Add(memoShard);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {memoShard.Id}. board = {memoShard.Layer}.");
                        Logger.Debug($"New pickupbox added: id = {memoShard.Id}. board = {memoShard.Layer}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoShard: id = {memoShard.Id}. board = {memoShard.Layer}.");
                        Logger.Warn($"Failed to add new MermoShard: id = {memoShard.Id}. board = {memoShard.Layer}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoShard not found: id = {memoShard.Id}. board = {memoShard.Layer}.");
                    Logger.Warn($"MermoShard not found: id = {memoShard.Id}. board = {memoShard.Layer}.");
                }
            }
            return (saved > 0);
        }


        public async Task<int> RemoveAllByLayerAsync(ulong layer)
        {
            Logger.Trace($"RemoveAllByLayer() layer = {layer}.");

            var queryable = dbContext.MemoShards
                .Select(o => o)
                .Where(o => o.Layer == layer);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.MemoShards.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. layer = {layer}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. layer = {layer}.");
                return saved;
            }
            return 0;
        }
        public async Task<int> RemoveAllByBoardAsync(ulong board)
        {
            Logger.Trace($"RemoveAllByBoard() board = {board}.");

            var queryable = dbContext.MemoShards
                .Select(o => o)
                .Where(o => o.Layer == board);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.MemoShards.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. board = {board}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. board = {board}.");
                return saved;
            }
            return 0;
        }

        public async Task<MemoShard> DeleteItemByLayerAsync(ulong id, ulong layer = 0)
        {
            Logger.Trace($"DeleteById() layer = {layer}; id = {id}.");

            var memoShard = dbContext.MemoShards.FirstOrDefault(o => o.Layer == layer && o.Id == id);
            if (memoShard != null) {
                dbContext.MemoShards.Remove(memoShard);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: id = {memoShard.Id}. layer = {memoShard.Layer}.");
                    Logger.Debug($"MermoShard deleted: id = {memoShard.Id}. layer = {memoShard.Layer}.");
                    return memoShard;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: id = {memoShard.Id}. layer = {memoShard.Layer}.");
                    Logger.Warn($"Failed to deleted pickupbox: id = {memoShard.Id}. layer = {memoShard.Layer}.");
                    return null;
                }
            }
            return null;
        }
        public async Task<MemoShard> DeleteItemByBoardAsync(ulong id, ulong board = 0)
        {
            Logger.Trace($"DeleteItemByBoard() board = {board}; id = {id}.");

            var memoShard = dbContext.MemoShards.FirstOrDefault(o => o.Layer == board && o.Id == id);
            if (memoShard != null) {
                dbContext.MemoShards.Remove(memoShard);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: id = {memoShard.Id}. board = {memoShard.Layer}.");
                    Logger.Debug($"MermoShard deleted: id = {memoShard.Id}. board = {memoShard.Layer}.");
                    return memoShard;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: id = {memoShard.Id}. board = {memoShard.Layer}.");
                    Logger.Warn($"Failed to deleted pickupbox: id = {memoShard.Id}. board = {memoShard.Layer}.");
                    return null;
                }
            }
            return null;
        }



    }
}
