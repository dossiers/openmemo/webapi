﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using HoloLC.MemoDataLib.DB;

namespace HoloLC.MemoDataLib.Persistent
{
    public class PersistentMemoLayerRepository : IMemoLayerRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly MemoDbContext dbContext;

        public PersistentMemoLayerRepository(MemoDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public async Task<IEnumerable<MemoLayer>> FindAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"FindAllByOwner() owner = {owner}.");

            var queryable = dbContext.MemoLayers
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAllByOwner() returing null. owner = {owner}.");
            return null;
        }
        public async Task<IEnumerable<MemoLayer>> FindAllByBoardAsync(ulong board)
        {
            Logger.Trace($"FindAllByBoard() board = {board}.");

            var queryable = dbContext.MemoLayers
                .Select(o => o)
                .Where(o => o.Owner == board);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAllByBoard() returing null. board = {board}.");
            return null;
        }

        public async Task<MemoLayer> GetItemByOwnerAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"GetItemByOwner() id = {id}; owner = {owner}.");

            var memoLayer = dbContext.MemoLayers.FirstOrDefault(o => o.Id == id && o.Owner == owner);
            return memoLayer;
        }
        public async Task<MemoLayer> GetItemByBoardAsync(ulong id, ulong board = 0)
        {
            Logger.Trace($"GetItemByBoard() id = {id}; board = {board}.");

            var memoLayer = dbContext.MemoLayers.FirstOrDefault(o => o.Id == id && o.Owner == board);
            return memoLayer;
        }

        public async Task<bool> AddItemAsync(MemoLayer memoLayer)
        {
            Logger.Trace($"AddItem() pmemoLayer = {memoLayer}.");

            dbContext.MemoLayers.Add(memoLayer);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoLayer saved: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                Logger.Debug($"MermoLayer saved: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoLayer: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                Logger.Warn($"Failed to save MermoLayer: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                return false;
            }
        }

        public async Task<bool> ReplaceItemAsync(MemoLayer memoLayer, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() pmemoLayer = {memoLayer}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingMemoLayer = dbContext.MemoLayers.FirstOrDefault(o => o.Owner == memoLayer.Owner && o.Id == memoLayer.Id);
            if (existingMemoLayer != null) {
                existingMemoLayer.CopyDataFrom(memoLayer);
                dbContext.MemoLayers.Update(existingMemoLayer);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoLayer replaced: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                    Logger.Debug($"MermoLayer replaced: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoLayer: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                    Logger.Warn($"Failed to replace MermoLayer: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.MemoLayers.Add(memoLayer);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                        Logger.Debug($"New pickupbox added: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoLayer: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                        Logger.Warn($"Failed to add new MermoLayer: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoLayer not found: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                    Logger.Warn($"MermoLayer not found: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                }
            }
            return (saved > 0);
        }


        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"RemoveAllByOwner() owner = {owner}.");

            var queryable = dbContext.MemoLayers
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.MemoLayers.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoLayer removed: target count = {count}; removed = {saved}. owner = {owner}.");
                Logger.Debug($"MermoLayer removed: target count = {count}; removed = {saved}. owner = {owner}.");
                return saved;
            }
            return 0;
        }
        public async Task<int> RemoveAllByBoardAsync(ulong board)
        {
            Logger.Trace($"RemoveAllByBoard() board = {board}.");

            var queryable = dbContext.MemoLayers
                .Select(o => o)
                .Where(o => o.Owner == board);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.MemoLayers.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoLayer removed: target count = {count}; removed = {saved}. board = {board}.");
                Logger.Debug($"MermoLayer removed: target count = {count}; removed = {saved}. board = {board}.");
                return saved;
            }
            return 0;
        }

        public async Task<MemoLayer> DeleteItemByOwnerAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"DeleteById() owner = {owner}; id = {id}.");

            var memoLayer = dbContext.MemoLayers.FirstOrDefault(o => o.Owner == owner && o.Id == id);
            if (memoLayer != null) {
                dbContext.MemoLayers.Remove(memoLayer);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoLayer deleted: id = {memoLayer.Id}. owner = {memoLayer.Owner}.");
                    Logger.Debug($"MermoLayer deleted: id = {memoLayer.Id}. owner = {memoLayer.Owner}.");
                    return memoLayer;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: id = {memoLayer.Id}. owner = {memoLayer.Owner}.");
                    Logger.Warn($"Failed to deleted pickupbox: id = {memoLayer.Id}. owner = {memoLayer.Owner}.");
                    return null;
                }
            }
            return null;
        }
        public async Task<MemoLayer> DeleteItemByBoardAsync(ulong id, ulong board = 0)
        {
            Logger.Trace($"DeleteItemByBoard() board = {board}; id = {id}.");

            var memoLayer = dbContext.MemoLayers.FirstOrDefault(o => o.Owner == board && o.Id == id);
            if (memoLayer != null) {
                dbContext.MemoLayers.Remove(memoLayer);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoLayer deleted: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                    Logger.Debug($"MermoLayer deleted: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                    return memoLayer;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                    Logger.Warn($"Failed to deleted pickupbox: id = {memoLayer.Id}. board = {memoLayer.Owner}.");
                    return null;
                }
            }
            return null;
        }



    }
}
