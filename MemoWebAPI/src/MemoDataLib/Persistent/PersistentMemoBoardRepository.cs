﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using HoloLC.MemoDataLib.DB;

namespace HoloLC.MemoDataLib.Persistent
{
    public class PersistentMemoBoardRepository : IMemoBoardRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly MemoDbContext dbContext;

        public PersistentMemoBoardRepository(MemoDbContext dbContext)
        {
            this.dbContext = dbContext;
        }



        public async Task<IEnumerable<MemoBoard>> FindAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"FindAll() owner = {owner}.");

            var queryable = dbContext.MemoBoards
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAll() returing null. owner = {owner}.");
            return null;
        }

        public async Task<MemoBoard> GetItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"GetItem() id = {id}; owner = {owner}.");

            var memoBoard = dbContext.MemoBoards.FirstOrDefault(o => o.Id == id && o.Owner == owner);
            return memoBoard;
        }

        public async Task<bool> AddItemAsync(MemoBoard memoBoard)
        {
            Logger.Trace($"AddItem() pmemoBoard = {memoBoard}.");

            dbContext.MemoBoards.Add(memoBoard);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoShard saved: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                Logger.Debug($"MermoShard saved: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoShard: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                Logger.Warn($"Failed to save MermoShard: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                return false;
            }
        }

        public async Task<bool> ReplaceItemAsync(MemoBoard memoBoard, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() pmemoBoard = {memoBoard}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingMemoBoard = dbContext.MemoBoards.FirstOrDefault(o => o.Owner == memoBoard.Owner && o.Id == memoBoard.Id);
            if (existingMemoBoard != null) {
                existingMemoBoard.CopyDataFrom(memoBoard);
                dbContext.MemoBoards.Update(existingMemoBoard);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard replaced: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                    Logger.Debug($"MermoShard replaced: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoShard: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                    Logger.Warn($"Failed to replace MermoShard: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.MemoBoards.Add(memoBoard);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                        Logger.Debug($"New pickupbox added: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoShard: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                        Logger.Warn($"Failed to add new MermoShard: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoShard not found: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                    Logger.Warn($"MermoShard not found: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                }
            }
            return (saved > 0);
        }


        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            Logger.Trace($"RemoveAll() owner = {owner}.");

            var queryable = dbContext.MemoBoards
                .Select(o => o)
                .Where(o => o.Owner == owner);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.MemoBoards.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. owner = {owner}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. owner = {owner}.");
                return saved;
            }
            return 0;
        }

        public async Task<MemoBoard> DeleteItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"DeleteById() owner = {owner}; id = {id}.");

            var memoBoard = dbContext.MemoBoards.FirstOrDefault(o => o.Owner == owner && o.Id == id);
            if (memoBoard != null) {
                dbContext.MemoBoards.Remove(memoBoard);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                    Logger.Debug($"MermoShard deleted: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                    return memoBoard;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                    Logger.Warn($"Failed to deleted pickupbox: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                    return null;
                }
            }
            return null;
        }

    }
}
