﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using HoloLC.MemoDataLib.DB;

namespace HoloLC.MemoDataLib.Persistent
{
    public class PersistentBoardChildRepository : IBoardChildRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly MemoDbContext dbContext;

        public PersistentBoardChildRepository(MemoDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public async Task<IEnumerable<BoardChild>> FindChildrenAsync(ulong parentId)
        {
            Logger.Trace($"FindChildren() parentId = {parentId}.");

            var queryable = dbContext.BoardChildren
                .Select(o => o)
                .Where(o => o.ParentId == parentId);
            if (queryable != null) {
                return queryable.ToList();
            }
            Logger.Info($"FindAll() returing null. parentId = {parentId}.");
            return null;
        }

        public async Task<bool> AddBoardChildAsync(BoardChild boardChild)
        {
            Logger.Trace($"AddBoardChild() boardChild = {boardChild}.");

            dbContext.BoardChildren.Add(boardChild);
            var saved = dbContext.SaveChanges();
            if (saved > 0) {
                System.Diagnostics.Debug.WriteLine($"MermoShard saved: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                Logger.Debug($"MermoShard saved: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                return true;
            } else {
                System.Diagnostics.Debug.WriteLine($"Failed to save MermoShard: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                Logger.Warn($"Failed to save MermoShard: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                return false;
            }
        }

        public async Task<bool> ReplaceBoardChildAsync(BoardChild boardChild, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItem() boardChild = {boardChild}; createIfNotExist = {createIfNotExist}.");

            int saved = 0;
            var existingBoardChild = dbContext.BoardChildren.FirstOrDefault(o => o.ParentId == boardChild.ParentId && o.ChildId == boardChild.ChildId);
            if (existingBoardChild != null) {
                existingBoardChild.CopyDataFrom(boardChild);
                dbContext.BoardChildren.Update(existingBoardChild);
                saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard replaced: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                    Logger.Debug($"MermoShard replaced: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to replace MermoShard: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                    Logger.Warn($"Failed to replace MermoShard: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                }
            } else {
                if (createIfNotExist) {
                    dbContext.BoardChildren.Add(boardChild);
                    saved = dbContext.SaveChanges();
                    if (saved > 0) {
                        System.Diagnostics.Debug.WriteLine($"New pickupbox added: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                        Logger.Debug($"New pickupbox added: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                    } else {
                        System.Diagnostics.Debug.WriteLine($"Failed to add new MermoShard: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                        Logger.Warn($"Failed to add new MermoShard: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                    }
                } else {
                    System.Diagnostics.Debug.WriteLine($"MermoShard not found: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                    Logger.Warn($"MermoShard not found: id = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                }
            }
            return (saved > 0);
        }

        public async Task<int> RemoveByParentAsync(ulong parentId)
        {
            Logger.Trace($"RemoveByParent() parentId = {parentId}.");

            var queryable = dbContext.BoardChildren
                .Select(o => o)
                .Where(o => o.ParentId == parentId);
            if (queryable != null) {
                var list = queryable.ToList();
                var count = list.Count;
                dbContext.BoardChildren.RemoveRange(list);
                var saved = dbContext.SaveChanges();
                System.Diagnostics.Debug.WriteLine($"MermoShard removed: target count = {count}; removed = {saved}. parentId = {parentId}.");
                Logger.Debug($"MermoShard removed: target count = {count}; removed = {saved}. parentId = {parentId}.");
                return saved;
            }
            return 0;
        }

        public async Task<BoardChild> DeleteBoardChildAsync(ulong parentId, ulong childId)
        {
            Logger.Trace($"DeleteById() parentId = {parentId}; childId = {childId}.");

            var boardChild = dbContext.BoardChildren.FirstOrDefault(o => o.ParentId == parentId && o.ChildId == childId);
            if (boardChild != null) {
                dbContext.BoardChildren.Remove(boardChild);
                var saved = dbContext.SaveChanges();
                if (saved > 0) {
                    System.Diagnostics.Debug.WriteLine($"MermoShard deleted: childId = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                    Logger.Debug($"MermoShard deleted: childId = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                    return boardChild;
                } else {
                    System.Diagnostics.Debug.WriteLine($"Failed to deleted pickupbox: childId = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                    Logger.Warn($"Failed to deleted pickupbox: childId = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                    return null;
                }
            }
            return null;
        }

    }
}
