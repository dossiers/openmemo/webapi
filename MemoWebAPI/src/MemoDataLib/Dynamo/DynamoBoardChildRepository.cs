﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using HoloLC.MemoDataLib.Dynamo.Tables.Converters;
using Amazon.DynamoDBv2.DocumentModel;

namespace HoloLC.MemoDataLib.Dynamo
{
    public class DynamoBoardChildRepository : BaseDynamoRepository, IBoardChildRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoBoardChildRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }


        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<BoardChild>> FindChildrenAsync(ulong parentId)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("ParentId", QueryOperator.Equal, parentId),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var boardChildren = new List<BoardChild>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = BoardChildConverter.ConvertFromDocument(d);
                            if (box != null) {
                                boardChildren.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to BoardChild. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"boardChildren feteched. Count = {boardChildren.Count}");
                    return boardChildren;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindMemoBoardsAsync() (binder = {parentId}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddBoardChildAsync(BoardChild boardChild)
        {
            Logger.Trace($"AddBoardChildAsync() boardChild = {boardChild}.");

            try {
                var document = BoardChildConverter.ConvertToDocument(boardChild);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (boardChild != null) {
                        Logger.Debug($"BoardChild saved: parentId = {boardChild.ParentId}. childId = {boardChild.ChildId}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddBoardChildAsync() (boardChild = {boardChild}) failed. {ex.Message}");
            }
            Logger.Info($"AddBoardChildAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceBoardChildAsync(BoardChild boardChild, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceBoardChildAsync() boardChild = {boardChild}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var parentId = boardChild.ParentId;
                var childId = boardChild.ChildId;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(parentId, childId);
                if (existingDocument != null) {
                    var newDocument = BoardChildConverter.ConvertToDocument(boardChild);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"BoardChild replaced: childId = {childId}. parentId = {parentId}.");
                    } else {
                        Logger.Warn($"Failed to replace BoardChild: childId = {childId}. parentId = {parentId}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = BoardChildConverter.ConvertToDocument(boardChild);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: owner = {childId}. id = {parentId}.");
                        } else {
                            Logger.Warn($"Failed to add new BoardChild: owner = {childId}. id = {parentId}.");
                        }
                    } else {
                        Logger.Info($"BoardChild not found: owner = {childId}. id = {parentId}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceBoardChildAsync() (boardChild = {boardChild}) failed. {ex.Message}");
            }
            return saved;
        }


        public async Task<BoardChild> DeleteBoardChildAsync(ulong parentId, ulong childId)
        {
            Logger.Trace($"DeleteBoardChildAsync() parentId = {parentId}; childId = {childId}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(parentId, childId);
                if (existingDocument != null) {
                    var boardChild = BoardChildConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (boardChild != null) {
                        Logger.Debug($"BoardChild deleted: childId = {boardChild.ChildId}. parentId = {boardChild.ParentId}.");
                        return boardChild;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: parentId = {parentId}; childId = {childId}.");
                        return null;
                    }
                } else {
                    Logger.Info($"BoardChild not found for parentId = {parentId}; childId = {childId}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteBoardChildAsync() (parentId = {parentId}; childId = {childId}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<int> RemoveByParentAsync(ulong parentId)
        {
            var count = 0;
            var list = await FindChildrenAsync(parentId);
            if (list != null) {
                foreach (var box in list) {
                    var d = BoardChildConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }


    }
}
