﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using HoloLC.MemoDataLib.Dynamo.Tables.Converters;
using Amazon.DynamoDBv2.DocumentModel;

namespace HoloLC.MemoDataLib.Dynamo
{
    public class DynamoMemoBinderRepository : BaseDynamoRepository, IMemoBinderRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoMemoBinderRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }


        public async Task<MemoBinder> GetItemAsync(ulong id, ulong owner = 0)
        {
            // tbd:
            MemoBinder memoBinder = null;
            try {
                var document = await DynamoTable.Table?.GetItemAsync(id);
                memoBinder = MemoBinderConverter.ConvertFromDocument(document);
                Logger.Debug($"id = {id}. memoBinder found: {memoBinder}.");
            } catch (Exception ex) {
                Logger.Warn($"GetItemAsync() DynamoTable.Table?.GetItemAsync({id}) failed. {ex.Message}");
            }
            return memoBinder;
        }

        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<MemoBinder>> FindAllByOwnerAsync(ulong owner)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("Owner", QueryOperator.Equal, owner),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var memoBinders = new List<MemoBinder>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = MemoBinderConverter.ConvertFromDocument(d);
                            if (box != null) {
                                memoBinders.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to MemoBinder. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"memoBinders feteched. Count = {memoBinders.Count}");
                    return memoBinders;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindAllAsync() (owner = {owner}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddItemAsync(MemoBinder memoBinder)
        {
            Logger.Trace($"AddItemAsync() memoBinder = {memoBinder}.");

            try {
                var document = MemoBinderConverter.ConvertToDocument(memoBinder);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (memoBinder != null) {
                        Logger.Debug($"MemoBinder saved: id = {memoBinder.Id}. owner = {memoBinder.Owner}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddItemAsync() (memoBinder = {memoBinder}) failed. {ex.Message}");
            }
            Logger.Info($"AddItemAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceItemAsync(MemoBinder memoBinder, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItemAsync() memoBinder = {memoBinder}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var id = memoBinder.Id;
                var owner = memoBinder.Owner;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id, owner);
                if (existingDocument != null) {
                    var newDocument = MemoBinderConverter.ConvertToDocument(memoBinder);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"MemoBinder replaced: owner = {owner}. id = {id}.");
                    } else {
                        Logger.Warn($"Failed to replace MemoBinder: owner = {owner}. id = {id}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = MemoBinderConverter.ConvertToDocument(memoBinder);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: owner = {owner}. id = {id}.");
                        } else {
                            Logger.Warn($"Failed to add new MemoBinder: owner = {owner}. id = {id}.");
                        }
                    } else {
                        Logger.Info($"MemoBinder not found: owner = {owner}. id = {id}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceItemAsync() (memoBinder = {memoBinder}) failed. {ex.Message}");
            }
            return saved;
        }


        public async Task<MemoBinder> DeleteItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"DeleteItemAsync() id = {id}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id);
                if (existingDocument != null) {
                    var memoBinder = MemoBinderConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (memoBinder != null) {
                        Logger.Debug($"MemoBinder deleted: owner = {memoBinder.Owner}. id = {memoBinder.Id}.");
                        return memoBinder;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: id = {id}.");
                        return null;
                    }
                } else {
                    Logger.Info($"MemoBinder not found for id = {id}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteItemAsync() (id = {id}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            var count = 0;
            var list = await FindAllByOwnerAsync(owner);
            if (list != null) {
                foreach (var box in list) {
                    var d = MemoBinderConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
