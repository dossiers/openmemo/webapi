﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.MemoDataLib.Dynamo.Tables;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using HoloLC.MemoDataLib.Dynamo.Tables.Converters;

namespace HoloLC.MemoDataLib.Dynamo
{

    // http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DotNetSDKMidLevel.html

    public class DynamoMemoShardRepository : BaseDynamoRepository, IMemoShardRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoMemoShardRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }


        private async Task<MemoShard> GetItemAsync(ulong id)
        {
            MemoShard memoShard = null;
            try {
                var document = await DynamoTable.Table?.GetItemAsync(id);
                memoShard = MemoShardConverter.ConvertFromDocument(document);
                Logger.Debug($"id = {id}. memoShard found: {memoShard}.");
            } catch (Exception ex) {
                Logger.Warn($"GetItemAsync() DynamoTable.Table?.GetItemAsync({id}) failed. {ex.Message}");
            }
            return memoShard;
        }

        public async Task<MemoShard> GetItemByBoardAsync(ulong id, ulong board = 0)
        {
            // tbd:
            if (board == 0) {
                return await GetItemAsync(id);
            } else {
                MemoShard memoShard = null;
                try {
                    var document = await DynamoTable.Table?.GetItemAsync(id, board);
                    memoShard = MemoShardConverter.ConvertFromDocument(document);
                    Logger.Debug($"id = {id}; board = {board}. memoShard found: {memoShard}.");
                } catch (Exception ex) {
                    Logger.Warn($"GetItemByBoardAsync() DynamoTable.Table?.GetItemAsync({id},{board}) failed. {ex.Message}");
                }
                return memoShard;
            }
        }

        public async Task<MemoShard> GetItemByLayerAsync(ulong id, ulong layer = 0)
        {
            // tbd:
            return await GetItemAsync(id);
        }


        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<MemoShard>> FindAllByBoardAsync(ulong board)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("Board", QueryOperator.Equal, board),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var memoShards = new List<MemoShard>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = MemoShardConverter.ConvertFromDocument(d);
                            if (box != null) {
                                memoShards.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to MemoShard. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"memoShards feteched. Count = {memoShards.Count}");
                    return memoShards;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindAllAsync() (board = {board}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }

        public async Task<IEnumerable<MemoShard>> FindAllByLayerAsync(ulong layer)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("Layer", QueryOperator.Equal, layer),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var memoShards = new List<MemoShard>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = MemoShardConverter.ConvertFromDocument(d);
                            if (box != null) {
                                memoShards.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to MemoShard. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"memoShards feteched. Count = {memoShards.Count}");
                    return memoShards;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindAllAsync() (layer = {layer}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddItemAsync(MemoShard memoShard)
        {
            Logger.Trace($"AddItemAsync() memoShard = {memoShard}.");

            try {
                var document = MemoShardConverter.ConvertToDocument(memoShard);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (memoShard != null) {
                        Logger.Debug($"MemoShard saved: id = {memoShard.Id}. board = {memoShard.Board}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddItemAsync() (memoShard = {memoShard}) failed. {ex.Message}");
            }
            Logger.Info($"AddItemAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceItemAsync(MemoShard memoShard, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItemAsync() memoShard = {memoShard}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var id = memoShard.Id;
                var board = memoShard.Board;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id, board);
                if (existingDocument != null) {
                    var newDocument = MemoShardConverter.ConvertToDocument(memoShard);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"MemoShard replaced: board = {board}. id = {id}.");
                    } else {
                        Logger.Warn($"Failed to replace MemoShard: board = {board}. id = {id}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = MemoShardConverter.ConvertToDocument(memoShard);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: board = {board}. id = {id}.");
                        } else {
                            Logger.Warn($"Failed to add new MemoShard: board = {board}. id = {id}.");
                        }
                    } else {
                        Logger.Info($"MemoShard not found: board = {board}. id = {id}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceItemAsync() (memoShard = {memoShard}) failed. {ex.Message}");
            }
            return saved;
        }


        private async Task<MemoShard> DeleteItemAsync(ulong id)
        {
            Logger.Trace($"DeleteItemAsync() id = {id}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id);
                if (existingDocument != null) {
                    var memoShard = MemoShardConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (memoShard != null) {
                        Logger.Debug($"MemoShard deleted: board = {memoShard.Board}. id = {memoShard.Id}.");
                        return memoShard;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: id = {id}.");
                        return null;
                    }
                } else {
                    Logger.Info($"MemoShard not found for id = {id}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteItemAsync() (id = {id}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<MemoShard> DeleteItemByBoardAsync(ulong id, ulong board = 0)
        {
            Logger.Trace($"DeleteItemByBoardAsync() id = {id}; board = {board}.");

            // tbd:
            if (board == 0) {
                return await DeleteItemAsync(id);
            } else {
                try {
                    var existingDocument = await DynamoTable.Table?.GetItemAsync(id, board);
                    if (existingDocument != null) {
                        var memoShard = MemoShardConverter.ConvertFromDocument(existingDocument);
                        await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                        // tbd: How do you know if DeleteItemAsync() was successful???
                        if (memoShard != null) {
                            Logger.Debug($"MemoShard deleted: board = {memoShard.Board}. id = {memoShard.Id}.");
                            return memoShard;
                        } else {
                            Logger.Warn($"Failed to delete pickupbox: board = {board}. id = {id}.");
                            return null;
                        }
                    } else {
                        Logger.Info($"MemoShard not found for board = {board}. id = {id}.");
                        return null;
                    }
                } catch (Exception ex) {
                    Logger.Warn($"DeleteItemByBoardAsync() (id = {id}; board = {board}) failed. {ex.Message}");
                    return null;
                }
            }
        }

        public async Task<MemoShard> DeleteItemByLayerAsync(ulong id, ulong layer = 0)
        {
            // tbd:
            return await DeleteItemAsync(id);
        }


        public async Task<int> RemoveAllByBoardAsync(ulong board)
        {
            var count = 0;
            var list = await FindAllByBoardAsync(board);
            if (list != null) {
                foreach (var box in list) {
                    var d = MemoShardConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

        public async Task<int> RemoveAllByLayerAsync(ulong layer)
        {
            var count = 0;
            var list = await FindAllByLayerAsync(layer);
            if (list != null) {
                foreach (var box in list) {
                    var d = MemoShardConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
