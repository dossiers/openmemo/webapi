﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using HoloLC.MemoDataLib.Dynamo.Tables.Converters;
using Amazon.DynamoDBv2.DocumentModel;

namespace HoloLC.MemoDataLib.Dynamo
{
    public class DynamoMemoBoardRepository : BaseDynamoRepository, IMemoBoardRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoMemoBoardRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }


        public async Task<MemoBoard> GetItemAsync(ulong id, ulong owner = 0)
        {
            // tbd:
            MemoBoard memoBoard = null;
            try {
                var document = await DynamoTable.Table?.GetItemAsync(id);
                memoBoard = MemoBoardConverter.ConvertFromDocument(document);
                Logger.Debug($"id = {id}. memoBoard found: {memoBoard}.");
            } catch (Exception ex) {
                Logger.Warn($"GetItemAsync() DynamoTable.Table?.GetItemAsync({id}) failed. {ex.Message}");
            }
            return memoBoard;
        }

        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<MemoBoard>> FindAllByOwnerAsync(ulong owner)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("Owner", QueryOperator.Equal, owner),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var memoBoards = new List<MemoBoard>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = MemoBoardConverter.ConvertFromDocument(d);
                            if (box != null) {
                                memoBoards.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to MemoBoard. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"memoBoards feteched. Count = {memoBoards.Count}");
                    return memoBoards;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindAllAsync() (owner = {owner}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddItemAsync(MemoBoard memoBoard)
        {
            Logger.Trace($"AddItemAsync() memoBoard = {memoBoard}.");

            try {
                var document = MemoBoardConverter.ConvertToDocument(memoBoard);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (memoBoard != null) {
                        Logger.Debug($"MemoBoard saved: id = {memoBoard.Id}. owner = {memoBoard.Owner}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddItemAsync() (memoBoard = {memoBoard}) failed. {ex.Message}");
            }
            Logger.Info($"AddItemAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceItemAsync(MemoBoard memoBoard, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItemAsync() memoBoard = {memoBoard}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var id = memoBoard.Id;
                var owner = memoBoard.Owner;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id, owner);
                if (existingDocument != null) {
                    var newDocument = MemoBoardConverter.ConvertToDocument(memoBoard);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"MemoBoard replaced: owner = {owner}. id = {id}.");
                    } else {
                        Logger.Warn($"Failed to replace MemoBoard: owner = {owner}. id = {id}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = MemoBoardConverter.ConvertToDocument(memoBoard);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: owner = {owner}. id = {id}.");
                        } else {
                            Logger.Warn($"Failed to add new MemoBoard: owner = {owner}. id = {id}.");
                        }
                    } else {
                        Logger.Info($"MemoBoard not found: owner = {owner}. id = {id}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceItemAsync() (memoBoard = {memoBoard}) failed. {ex.Message}");
            }
            return saved;
        }


        public async Task<MemoBoard> DeleteItemAsync(ulong id, ulong owner = 0)
        {
            Logger.Trace($"DeleteItemAsync() id = {id}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id);
                if (existingDocument != null) {
                    var memoBoard = MemoBoardConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (memoBoard != null) {
                        Logger.Debug($"MemoBoard deleted: owner = {memoBoard.Owner}. id = {memoBoard.Id}.");
                        return memoBoard;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: id = {id}.");
                        return null;
                    }
                } else {
                    Logger.Info($"MemoBoard not found for id = {id}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteItemAsync() (id = {id}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            var count = 0;
            var list = await FindAllByOwnerAsync(owner);
            if (list != null) {
                foreach (var box in list) {
                    var d = MemoBoardConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
