﻿using HoloLC.MemoDataLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HoloLC.MemoCoreLib.Models;
using NLog;
using AWSCore.DynamoBase.Tables.Core;
using AWSCore.DynamoBase.Repositories.Base;
using HoloLC.MemoDataLib.Dynamo.Tables.Converters;
using Amazon.DynamoDBv2.DocumentModel;

namespace HoloLC.MemoDataLib.Dynamo
{
    public class DynamoMemoLayerRepository : BaseDynamoRepository, IMemoLayerRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DynamoMemoLayerRepository(IDynamoTable dynamoTable)
            : base(dynamoTable)
        {
        }


        private async Task<MemoLayer> GetItemAsync(ulong id)
        {
            MemoLayer memoLayer = null;
            try {
                var document = await DynamoTable.Table?.GetItemAsync(id);
                memoLayer = MemoLayerConverter.ConvertFromDocument(document);
                Logger.Debug($"id = {id}. memoLayer found: {memoLayer}.");
            } catch (Exception ex) {
                Logger.Warn($"GetItemAsync() DynamoTable.Table?.GetItemAsync({id}) failed. {ex.Message}");
            }
            return memoLayer;
        }

        public async Task<MemoLayer> GetItemByBoardAsync(ulong id, ulong board = 0)
        {
            // tbd:
            if (board == 0) {
                return await GetItemAsync(id);
            } else {
                MemoLayer memoLayer = null;
                try {
                    var document = await DynamoTable.Table?.GetItemAsync(id, board);
                    memoLayer = MemoLayerConverter.ConvertFromDocument(document);
                    Logger.Debug($"id = {id}; board = {board}. memoLayer found: {memoLayer}.");
                } catch (Exception ex) {
                    Logger.Warn($"GetItemByBoardAsync() DynamoTable.Table?.GetItemAsync({id},{board}) failed. {ex.Message}");
                }
                return memoLayer;
            }
        }

        public async Task<MemoLayer> GetItemByOwnerAsync(ulong id, ulong owner = 0)
        {
            // tbd:
            return await GetItemAsync(id);
        }


        // cf. https://blogs.aws.amazon.com/net/post/Tx1KWKHIN0894B/DynamoDB-Document-Model-Manual-Pagination
        public async Task<IEnumerable<MemoLayer>> FindAllByBoardAsync(ulong board)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("Board", QueryOperator.Equal, board),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var memoLayers = new List<MemoLayer>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = MemoLayerConverter.ConvertFromDocument(d);
                            if (box != null) {
                                memoLayers.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to MemoLayer. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"memoLayers feteched. Count = {memoLayers.Count}");
                    return memoLayers;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindAllAsync() (board = {board}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }

        public async Task<IEnumerable<MemoLayer>> FindAllByOwnerAsync(ulong owner)
        {
            try {
                // TBD: No pagination at this point....
                //      --> This should be fixed ASAP.
                var search = DynamoTable.Table?.Query(new QueryOperationConfig {
                    Filter = new QueryFilter("Owner", QueryOperator.Equal, owner),
                    Limit = 1000,    // arbitrary, a large number to avoid pagination. TBD: Does it work??? What's the maximum value of Limit?
                });
                IList<Document> items = null;
                if (search != null) {
                    items = await search.GetNextSetAsync();
                }
                // var token = search.PaginationToken;
                if (items != null) {
                    var memoLayers = new List<MemoLayer>();
                    if (items.Any()) {
                        foreach (var d in items) {
                            var box = MemoLayerConverter.ConvertFromDocument(d);
                            if (box != null) {
                                memoLayers.Add(box);
                            } else {
                                // ???
                                Logger.Warn($"Failed to convert document to MemoLayer. document = {d}");
                            }
                        }
                    }
                    Logger.Debug($"memoLayers feteched. Count = {memoLayers.Count}");
                    return memoLayers;
                }
            } catch (Exception ex) {
                Logger.Warn($"FindAllAsync() (owner = {owner}) failed. {ex.Message}");
            }
            // ????
            Logger.Warn($"search.GetNextSetAsync() failed.");
            return null;
        }


        public async Task<bool> AddItemAsync(MemoLayer memoLayer)
        {
            Logger.Trace($"AddItemAsync() memoLayer = {memoLayer}.");

            try {
                var document = MemoLayerConverter.ConvertToDocument(memoLayer);
                if (document != null) {
                    await DynamoTable.Table?.UpdateItemAsync(document);
                    // TBD: How to check if UpdateItemAsync() was successful???
                    if (memoLayer != null) {
                        Logger.Debug($"MemoLayer saved: id = {memoLayer.Id}. board = {memoLayer.Board}.");
                        return true;
                    } else {
                        Logger.Info($"Table.UpdateItemAsync() failed.");
                        return false;
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"AddItemAsync() (memoLayer = {memoLayer}) failed. {ex.Message}");
            }
            Logger.Info($"AddItemAsync() failed.");
            return false;
        }

        public async Task<bool> ReplaceItemAsync(MemoLayer memoLayer, bool createIfNotExist = false)
        {
            Logger.Trace($"ReplaceItemAsync() memoLayer = {memoLayer}; createIfNotExist = {createIfNotExist}.");

            var saved = false;
            try {
                var id = memoLayer.Id;
                var board = memoLayer.Board;
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id, board);
                if (existingDocument != null) {
                    var newDocument = MemoLayerConverter.ConvertToDocument(memoLayer);
                    if (newDocument != null) {
                        // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        await DynamoTable.Table?.UpdateItemAsync(newDocument);
                    }
                    // tbd: How do you know if UpdateItemAsync() was successful???
                    if (newDocument != null) {
                        saved = true;
                        Logger.Debug($"MemoLayer replaced: board = {board}. id = {id}.");
                    } else {
                        Logger.Warn($"Failed to replace MemoLayer: board = {board}. id = {id}.");
                    }
                } else {
                    if (createIfNotExist) {
                        var newDocument = MemoLayerConverter.ConvertToDocument(memoLayer);
                        if (newDocument != null) {
                            // newDocument = await DynamoTable.Table?.UpdateItemAsync(newDocument);
                            await DynamoTable.Table?.UpdateItemAsync(newDocument);
                        }
                        // tbd: How do you know if UpdateItemAsync() was successful???
                        if (newDocument != null) {
                            saved = true;
                            Logger.Debug($"New pickupbox added: board = {board}. id = {id}.");
                        } else {
                            Logger.Warn($"Failed to add new MemoLayer: board = {board}. id = {id}.");
                        }
                    } else {
                        Logger.Info($"MemoLayer not found: board = {board}. id = {id}.");
                    }
                }
            } catch (Exception ex) {
                Logger.Warn($"ReplaceItemAsync() (memoLayer = {memoLayer}) failed. {ex.Message}");
            }
            return saved;
        }


        private async Task<MemoLayer> DeleteItemAsync(ulong id)
        {
            Logger.Trace($"DeleteItemAsync() id = {id}.");

            try {
                var existingDocument = await DynamoTable.Table?.GetItemAsync(id);
                if (existingDocument != null) {
                    var memoLayer = MemoLayerConverter.ConvertFromDocument(existingDocument);
                    await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                    // tbd: How do you know if DeleteItemAsync() was successful???
                    if (memoLayer != null) {
                        Logger.Debug($"MemoLayer deleted: board = {memoLayer.Board}. id = {memoLayer.Id}.");
                        return memoLayer;
                    } else {
                        Logger.Warn($"Failed to delete pickupbox: id = {id}.");
                        return null;
                    }
                } else {
                    Logger.Info($"MemoLayer not found for id = {id}.");
                    return null;
                }
            } catch (Exception ex) {
                Logger.Warn($"DeleteItemAsync() (id = {id}) failed. {ex.Message}");
                return null;
            }
        }

        public async Task<MemoLayer> DeleteItemByBoardAsync(ulong id, ulong board = 0)
        {
            Logger.Trace($"DeleteItemByBoardAsync() id = {id}; board = {board}.");

            // tbd:
            if (board == 0) {
                return await DeleteItemAsync(id);
            } else {
                try {
                    var existingDocument = await DynamoTable.Table?.GetItemAsync(id, board);
                    if (existingDocument != null) {
                        var memoLayer = MemoLayerConverter.ConvertFromDocument(existingDocument);
                        await DynamoTable.Table?.DeleteItemAsync(existingDocument);
                        // tbd: How do you know if DeleteItemAsync() was successful???
                        if (memoLayer != null) {
                            Logger.Debug($"MemoLayer deleted: board = {memoLayer.Board}. id = {memoLayer.Id}.");
                            return memoLayer;
                        } else {
                            Logger.Warn($"Failed to delete pickupbox: board = {board}. id = {id}.");
                            return null;
                        }
                    } else {
                        Logger.Info($"MemoLayer not found for board = {board}. id = {id}.");
                        return null;
                    }
                } catch (Exception ex) {
                    Logger.Warn($"DeleteItemByBoardAsync() (id = {id}; board = {board}) failed. {ex.Message}");
                    return null;
                }
            }
        }

        public async Task<MemoLayer> DeleteItemByOwnerAsync(ulong id, ulong owner = 0)
        {
            // tbd:
            return await DeleteItemAsync(id);
        }


        public async Task<int> RemoveAllByBoardAsync(ulong board)
        {
            var count = 0;
            var list = await FindAllByBoardAsync(board);
            if (list != null) {
                foreach (var box in list) {
                    var d = MemoLayerConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

        public async Task<int> RemoveAllByOwnerAsync(ulong owner)
        {
            var count = 0;
            var list = await FindAllByOwnerAsync(owner);
            if (list != null) {
                foreach (var box in list) {
                    var d = MemoLayerConverter.ConvertToDocument(box);
                    if (d != null) {
                        await DynamoTable.Table?.DeleteItemAsync(d);
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
