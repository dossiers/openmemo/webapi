﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Dynamo.Tables.Converters
{
    public class MemoLayerConverter
    {
        public static Document ConvertToDocument(MemoLayer memoLayer)
        {
            if (memoLayer != null) {
                var document = new Document();
                document.Add(nameof(memoLayer.Id), memoLayer.Id);
                document.Add(nameof(memoLayer.Owner), memoLayer.Owner);
                if (memoLayer.Title != null) document.Add(nameof(memoLayer.Title), memoLayer.Title);
                if (memoLayer.Description != null) document.Add(nameof(memoLayer.Description), memoLayer.Description);
                if (memoLayer.AccessLevel.ToName() != null) document.Add(nameof(memoLayer.AccessLevel), memoLayer.AccessLevel.ToName());
                document.Add(nameof(memoLayer.Board), memoLayer.Board);
                document.Add(nameof(memoLayer.ZOrder), memoLayer.ZOrder);
                document.Add(nameof(memoLayer.Width), memoLayer.Width);
                document.Add(nameof(memoLayer.Height), memoLayer.Height);
                document.Add(nameof(memoLayer.Created), memoLayer.Created);
                document.Add(nameof(memoLayer.Updated), memoLayer.Updated);
                document.Add(nameof(memoLayer.Deleted), memoLayer.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static MemoLayer ConvertFromDocument(Document document)
        {
            if (document != null) {
                var memoLayer = new MemoLayer();
                memoLayer.Id = document[nameof(memoLayer.Id)].AsULong();
                memoLayer.Owner = document.ContainsKey(nameof(memoLayer.Owner)) ? document[nameof(memoLayer.Owner)].AsULong() : 0UL;
                memoLayer.Title = document.ContainsKey(nameof(memoLayer.Title)) ? document[nameof(memoLayer.Title)].AsString() : "";
                memoLayer.Description = document.ContainsKey(nameof(memoLayer.Description)) ? document[nameof(memoLayer.Description)].AsString() : "";
                memoLayer.AccessLevel = document.ContainsKey(nameof(memoLayer.AccessLevel)) ? document[nameof(memoLayer.AccessLevel)].AsString().ToAccessLevel() : AccessLevel.Unknown;
                memoLayer.Board = document.ContainsKey(nameof(memoLayer.Board)) ? document[nameof(memoLayer.Board)].AsULong() : 0UL;
                memoLayer.ZOrder = document.ContainsKey(nameof(memoLayer.ZOrder)) ? document[nameof(memoLayer.ZOrder)].AsInt() : 0;
                memoLayer.Width = document.ContainsKey(nameof(memoLayer.Width)) ? document[nameof(memoLayer.Width)].AsUInt() : 0U;
                memoLayer.Height = document.ContainsKey(nameof(memoLayer.Height)) ? document[nameof(memoLayer.Height)].AsUInt() : 0U;
                memoLayer.Created = document.ContainsKey(nameof(memoLayer.Created)) ? document[nameof(memoLayer.Created)].AsLong() : 0L;
                memoLayer.Updated = document.ContainsKey(nameof(memoLayer.Updated)) ? document[nameof(memoLayer.Updated)].AsLong() : 0L;
                memoLayer.Deleted = document.ContainsKey(nameof(memoLayer.Deleted)) ? document[nameof(memoLayer.Deleted)].AsLong() : 0L;
                return memoLayer;
            } else {
                return null;
            }
        }

    }
}
