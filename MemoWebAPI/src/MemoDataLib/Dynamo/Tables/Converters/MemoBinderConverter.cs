﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Dynamo.Tables.Converters
{
    public static class MemoBinderConverter
    {
        public static Document ConvertToDocument(MemoBinder memoBinder)
        {
            if (memoBinder != null) {
                var document = new Document();
                document.Add(nameof(memoBinder.Id), memoBinder.Id);
                document.Add(nameof(memoBinder.Owner), memoBinder.Owner);
                if (memoBinder.Title != null) document.Add(nameof(memoBinder.Title), memoBinder.Title);
                if (memoBinder.Description != null) document.Add(nameof(memoBinder.Description), memoBinder.Description);
                if (memoBinder.AccessLevel.ToName() != null) document.Add(nameof(memoBinder.AccessLevel), memoBinder.AccessLevel.ToName());
                document.Add(nameof(memoBinder.HomeBoard), memoBinder.HomeBoard);
                document.Add(nameof(memoBinder.Width), memoBinder.Width);
                document.Add(nameof(memoBinder.Height), memoBinder.Height);
                document.Add(nameof(memoBinder.Created), memoBinder.Created);
                document.Add(nameof(memoBinder.Updated), memoBinder.Updated);
                document.Add(nameof(memoBinder.Deleted), memoBinder.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static MemoBinder ConvertFromDocument(Document document)
        {
            if (document != null) {
                var memoBinder = new MemoBinder();
                memoBinder.Id = document[nameof(memoBinder.Id)].AsULong();
                memoBinder.Owner = document.ContainsKey(nameof(memoBinder.Owner)) ? document[nameof(memoBinder.Owner)].AsULong() : 0UL;
                memoBinder.Title = document.ContainsKey(nameof(memoBinder.Title)) ? document[nameof(memoBinder.Title)].AsString() : "";
                memoBinder.Description = document.ContainsKey(nameof(memoBinder.Description)) ? document[nameof(memoBinder.Description)].AsString() : "";
                memoBinder.AccessLevel = document.ContainsKey(nameof(memoBinder.AccessLevel)) ? document[nameof(memoBinder.AccessLevel)].AsString().ToAccessLevel() : AccessLevel.Unknown;
                memoBinder.HomeBoard = document.ContainsKey(nameof(memoBinder.HomeBoard)) ? document[nameof(memoBinder.HomeBoard)].AsULong() : 0UL;
                memoBinder.Width = document.ContainsKey(nameof(memoBinder.Width)) ? document[nameof(memoBinder.Width)].AsUInt() : 0U;
                memoBinder.Height = document.ContainsKey(nameof(memoBinder.Height)) ? document[nameof(memoBinder.Height)].AsUInt() : 0U;
                memoBinder.Created = document.ContainsKey(nameof(memoBinder.Created)) ? document[nameof(memoBinder.Created)].AsLong() : 0L;
                memoBinder.Updated = document.ContainsKey(nameof(memoBinder.Updated)) ? document[nameof(memoBinder.Updated)].AsLong() : 0L;
                memoBinder.Deleted = document.ContainsKey(nameof(memoBinder.Deleted)) ? document[nameof(memoBinder.Deleted)].AsLong() : 0L;
                return memoBinder;
            } else {
                return null;
            }
        }

    }
}
