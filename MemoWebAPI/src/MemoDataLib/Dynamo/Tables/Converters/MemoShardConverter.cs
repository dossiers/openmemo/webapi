﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Dynamo.Tables.Converters
{
    public class MemoShardConverter
    {
        public static Document ConvertToDocument(MemoShard memoShard)
        {
            if (memoShard != null) {
                var document = new Document();
                document.Add(nameof(memoShard.Id), memoShard.Id);
                if (memoShard.Type.ToName() != null) document.Add(nameof(memoShard.Type), memoShard.Type.ToName());
                document.Add(nameof(memoShard.Board), memoShard.Board);
                document.Add(nameof(memoShard.Layer), memoShard.Layer);
                // document.Add(nameof(memoShard.Position", memoShard.Position);
                document.Add(nameof(memoShard.ZOrder), memoShard.ZOrder);
                document.Add(nameof(memoShard.Created), memoShard.Created);
                document.Add(nameof(memoShard.Updated), memoShard.Updated);
                document.Add(nameof(memoShard.Deleted), memoShard.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static MemoShard ConvertFromDocument(Document document)
        {
            if (document != null) {
                var memoShard = new MemoShard();
                memoShard.Id = document[nameof(memoShard.Id)].AsULong();
                memoShard.Type = document.ContainsKey(nameof(memoShard.Type)) ? document[nameof(memoShard.Type)].AsString().ToShardType() : ShardType.Unknown;
                memoShard.Board = document.ContainsKey(nameof(memoShard.Board)) ? document[nameof(memoShard.Board)].AsULong() : 0UL;
                memoShard.Layer = document.ContainsKey(nameof(memoShard.Layer)) ? document[nameof(memoShard.Layer)].AsULong() : 0UL;
                // memoShard.Position = (ElementPosition)document[nameof(memoShard.Position)].AsInt();
                memoShard.ZOrder = document.ContainsKey(nameof(memoShard.ZOrder)) ? document[nameof(memoShard.ZOrder)].AsInt() : 0;
                memoShard.Created = document.ContainsKey(nameof(memoShard.Created)) ? document[nameof(memoShard.Created)].AsLong() : 0L;
                memoShard.Updated = document.ContainsKey(nameof(memoShard.Updated)) ? document[nameof(memoShard.Updated)].AsLong() : 0L;
                memoShard.Deleted = document.ContainsKey(nameof(memoShard.Deleted)) ? document[nameof(memoShard.Deleted)].AsLong() : 0L;
                return memoShard;
            } else {
                return null;
            }
        }

    }
}