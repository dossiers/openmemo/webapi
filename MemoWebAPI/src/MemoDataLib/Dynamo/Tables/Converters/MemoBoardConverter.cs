﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.MemoCoreLib.Core;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Dynamo.Tables.Converters
{
    public static class MemoBoardConverter
    {
        public static Document ConvertToDocument(MemoBoard memoBoard)
        {
            if (memoBoard != null) {
                var document = new Document();
                document.Add(nameof(memoBoard.Id), memoBoard.Id);
                document.Add(nameof(memoBoard.Owner), memoBoard.Owner);
                if (memoBoard.Title != null) document.Add(nameof(memoBoard.Title), memoBoard.Title);
                if (memoBoard.Description != null) document.Add(nameof(memoBoard.Description), memoBoard.Description);
                if (memoBoard.AccessLevel.ToName() != null) document.Add(nameof(memoBoard.AccessLevel), memoBoard.AccessLevel.ToName());
                if (memoBoard.Label != null) document.Add(nameof(memoBoard.Label), memoBoard.Label);
                document.Add(nameof(memoBoard.IsTerminal), memoBoard.IsTerminal);
                document.Add(nameof(memoBoard.Width), memoBoard.Width);
                document.Add(nameof(memoBoard.Height), memoBoard.Height);
                document.Add(nameof(memoBoard.Created), memoBoard.Created);
                document.Add(nameof(memoBoard.Updated), memoBoard.Updated);
                document.Add(nameof(memoBoard.Deleted), memoBoard.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static MemoBoard ConvertFromDocument(Document document)
        {
            if (document != null) {
                var memoBoard = new MemoBoard();
                memoBoard.Id = document[nameof(memoBoard.Id)].AsULong();
                memoBoard.Owner = document.ContainsKey(nameof(memoBoard.Owner)) ? document[nameof(memoBoard.Owner)].AsULong() : 0UL;
                memoBoard.Title = document.ContainsKey(nameof(memoBoard.Title)) ? document[nameof(memoBoard.Title)].AsString() : "";
                memoBoard.Description = document.ContainsKey(nameof(memoBoard.Description)) ? document[nameof(memoBoard.Description)].AsString() : "";
                memoBoard.AccessLevel = document.ContainsKey(nameof(memoBoard.AccessLevel)) ? document[nameof(memoBoard.AccessLevel)].AsString().ToAccessLevel() : AccessLevel.Unknown;
                memoBoard.Label = document.ContainsKey(nameof(memoBoard.Label)) ? document[nameof(memoBoard.Label)].AsString() : "";
                memoBoard.IsTerminal = document.ContainsKey(nameof(memoBoard.IsTerminal)) ? document[nameof(memoBoard.IsTerminal)].AsBoolean() : false;
                memoBoard.Width = document.ContainsKey(nameof(memoBoard.Width)) ? document[nameof(memoBoard.Width)].AsUInt() : 0U;
                memoBoard.Height = document.ContainsKey(nameof(memoBoard.Height)) ? document[nameof(memoBoard.Height)].AsUInt() : 0U;
                memoBoard.Created = document.ContainsKey(nameof(memoBoard.Created)) ? document[nameof(memoBoard.Created)].AsLong() : 0L;
                memoBoard.Updated = document.ContainsKey(nameof(memoBoard.Updated)) ? document[nameof(memoBoard.Updated)].AsLong() : 0L;
                memoBoard.Deleted = document.ContainsKey(nameof(memoBoard.Deleted)) ? document[nameof(memoBoard.Deleted)].AsLong() : 0L;
                return memoBoard;
            } else {
                return null;
            }
        }

    }
}
