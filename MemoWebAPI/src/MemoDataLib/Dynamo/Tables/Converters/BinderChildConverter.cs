﻿using Amazon.DynamoDBv2.DocumentModel;
using HoloLC.MemoCoreLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Dynamo.Tables.Converters
{
    public static class BinderChildConverter
    {
        public static Document ConvertToDocument(BinderChild binderChild)
        {
            if (binderChild != null) {
                var document = new Document();
                document.Add(nameof(binderChild.BinderId), binderChild.BinderId);
                document.Add(nameof(binderChild.BoardId), binderChild.BoardId);
                if(binderChild.BoardTitle != null) document.Add(nameof(binderChild.BoardTitle), binderChild.BoardTitle);
                document.Add(nameof(binderChild.Created), binderChild.Created);
                document.Add(nameof(binderChild.Updated), binderChild.Updated);
                document.Add(nameof(binderChild.Deleted), binderChild.Deleted);
                return document;
            } else {
                return null;
            }
        }
        public static BinderChild ConvertFromDocument(Document document)
        {
            if (document != null) {
                var binderChild = new BinderChild();
                binderChild.BinderId = document[nameof(binderChild.BinderId)].AsULong();
                binderChild.BoardId = document[nameof(binderChild.BoardId)].AsULong();
                binderChild.BoardTitle = document.ContainsKey(nameof(binderChild.BoardTitle)) ? document[nameof(binderChild.BoardTitle)].AsString() : "";
                binderChild.Created = document.ContainsKey(nameof(binderChild.Created)) ? document[nameof(binderChild.Created)].AsLong() : 0L;
                binderChild.Updated = document.ContainsKey(nameof(binderChild.Updated)) ? document[nameof(binderChild.Updated)].AsLong() : 0L;
                binderChild.Deleted = document.ContainsKey(nameof(binderChild.Deleted)) ? document[nameof(binderChild.Deleted)].AsLong() : 0L;
                return binderChild;
            } else {
                return null;
            }
        }

    }
}
