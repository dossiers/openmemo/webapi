﻿using Amazon.DynamoDBv2.Model;
using AWSCore.DynamoBase.Clients;
using AWSCore.DynamoBase.Tables.Base;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HoloLC.MemoDataLib.Dynamo.Tables
{
    public sealed class DynamoMemoLayerTable : BaseDynamoTable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public const string TABLE_KEY = "MemoData.MemoLayer";
        private const string DEFAULT_TABLE_NAME = TABLE_KEY;

        public DynamoMemoLayerTable(IDynamoDBClientContext clientContext, string name = null)
            : base(clientContext, TABLE_KEY, (name != null) ? name : DEFAULT_TABLE_NAME)
        {
        }

        public override async Task CreateAsync(bool recreateIfPresent = false)
        {
            try {
                var creating = false;
                var isCurrentlyPresent = await IsPresentAsync();
                if (recreateIfPresent) {
                    if (isCurrentlyPresent == true) {
                        await DeleteAsync();
                    }
                    creating = true;
                } else {
                    if (isCurrentlyPresent == false) {
                        creating = true;
                    }
                }

                if (creating) {
                    var request = new CreateTableRequest {
                        TableName = Name,
                        // TBD:
                        AttributeDefinitions = new List<AttributeDefinition>() {
                            new AttributeDefinition
                            {
                              AttributeName = "Id",
                              AttributeType = "N"
                            },
                            new AttributeDefinition
                            {
                              AttributeName = "Board",
                              AttributeType = "N"
                            },
                            new AttributeDefinition
                            {
                              AttributeName = "Owner",
                              AttributeType = "N"
                            },
                          },
                        KeySchema = new List<KeySchemaElement>() {
                            new KeySchemaElement
                            {
                              AttributeName = "Id",
                              KeyType = "HASH"  //Partition key
                            },
                            new KeySchemaElement
                            {
                              AttributeName = "Board",
                              KeyType = "RANGE"  // Sort key
                            }
                          },
                        ProvisionedThroughput = new ProvisionedThroughput {
                            ReadCapacityUnits = 2L,
                            WriteCapacityUnits = 1L
                        },
                        GlobalSecondaryIndexes = { new GlobalSecondaryIndex
                        {
                            IndexName = Name + "-Id-Owner-index",
                            Projection = new Projection
                            {
                                ProjectionType = "ALL"
                            },
                            KeySchema = new List<KeySchemaElement>()
                            {
                                new KeySchemaElement
                                {
                                    AttributeName = "Id",
                                    KeyType = "HASH"  //Partition key
                                },
                                new KeySchemaElement
                                {
                                    AttributeName = "Owner",
                                    KeyType = "RANGE"  // Sort key
                                }
                            },
                            ProvisionedThroughput = new ProvisionedThroughput
                            {
                                ReadCapacityUnits = 2L,
                                WriteCapacityUnits = 1L
                            }
                        } }
                    };
                    var response = await ClientContext.Client.CreateTableAsync(request);
                    var result = response.HttpStatusCode;
                    var tableDescription = response.TableDescription;
                    Console.WriteLine("{1}: {0} \t ReadCapacityUnits: {2} \t WriteCapacityUnits: {3}",
                                    tableDescription.TableStatus,
                                    tableDescription.TableName,
                                    tableDescription.ProvisionedThroughput.ReadCapacityUnits,
                                    tableDescription.ProvisionedThroughput.WriteCapacityUnits);

                    string status = tableDescription.TableStatus;
                    Console.WriteLine(Name + " - " + status);
                }
            } catch (Exception ex) {
                // What to do???
                Logger.Warn($"Failed to create a table, {Name}. {ex.Message}");
            }
        }

    }
}
