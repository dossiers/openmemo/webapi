﻿using MemoDataLibTests.Dynamo.Base;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoDataLibTests.Dynamo
{
    [TestFixture]
    public class DynamoMemoBoardRepositoryTests : BaseDynamoRepositoryTests
    {
        [SetUp]
        public override async Task InitAllAsync()
        {
            await base.InitAllAsync();
            await SetUpTestDataAsync();
        }

        [TearDown]
        public override async Task CleanUpAsync()
        {
            await base.CleanUpAsync();
        }

        private async Task SetUpTestDataAsync()
        {
            System.Diagnostics.Trace.WriteLine("Calling SetUpTestDataAsync()");

        }

        [TestCase(1LU)]
        public async Task TestGetItemAsync(ulong id)
        {
            System.Diagnostics.Trace.WriteLine($"Calling TestGetItemAsync(): id = {id}");

        }

    }
}
