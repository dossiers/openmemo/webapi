﻿using HoloLC.MemoDataLib.Managers;
using HoloLC.MemoDataLib.Managers.Impl;
using HoloLC.MemoDataLib.Registries;
using MemoDataLibTests.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoDataLibTests.Dynamo.Base
{
    public abstract class BaseDynamoRepositoryTests
    {
        private static CustomTraceListener traceListener = new CustomTraceListener();
        private IDataRepositoryManager repositoryManager;
        protected IDataRepositoryManager RepositoryManager
        {
            get
            {
                return repositoryManager;
            }
        }

        public virtual async Task InitAllAsync()
        {
            // System.Diagnostics.Trace.Listeners.Add(traceListener);
            System.Diagnostics.Trace.WriteLine("Calling DynamoManagerCodeRepositoryTests.InitAll()");

            // Note:
            // These tests require DynamoDB Local be running at the port, 8000.
            // Or,
            // if the aws dynamodb is used, 
            //    then appropriate credentials need to be set in aws-credentials file.
            // ....

            var dynamoServiceURL = "http://localhost:8000";
            // var dynamoServiceURL = "https://dynamodb.us-east-1.amazonaws.com/";
            // var dynamoServiceURL = "https://dynamodb.us-west-2.amazonaws.com/";
            var schemaVersion = "1";
            var envSuffix = "Test1";
            var tableSuffix = $"V{schemaVersion}{((envSuffix != null && envSuffix.Any()) ? "." + envSuffix : "")}";
            var tableCollection = TableCollectionRegistry.Instance.GetTableCollection(dynamoServiceURL, tableSuffix);
            // await (tableCollection as IInstantTableCollection).PurgeAllTablesAsync();
            repositoryManager = new DynamoRepositoryManager(tableCollection);
            // await (repositoryManager as IInstantRepositoryManager)?.PurgeAllAsync();
            await (repositoryManager as IInstantRepositoryManager)?.CreateAllAsync(true);
            await Task.Delay(2500);
            System.Diagnostics.Trace.WriteLine("repositoryManager created.");
            await Task.Delay(1000);
        }

        public virtual async Task CleanUpAsync()
        {
            await Task.Delay(1000);
            await (repositoryManager as IInstantRepositoryManager)?.DeleteAllAsync();
            System.Diagnostics.Trace.WriteLine("CleanUp(). Repository table cleaned.");
            // System.Diagnostics.Trace.Listeners.Remove(traceListener);
        }

    }
}
