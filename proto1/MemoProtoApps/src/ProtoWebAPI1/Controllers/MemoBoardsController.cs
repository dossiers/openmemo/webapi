﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MemoProto.MemoData1.Managers;
using MemoProto.MemoData1.Repositories;
using MemoProto.CoreLib1.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MemoProto.ProtoWebAPI1.Controllers
{
    [Route("api/user/{owner}/[controller]")]
    public class MemoBoardsController : Controller
    {
        public MemoBoardsController(IRepositoryManager repositoryManager)
        {
            MemoBoardRepository = repositoryManager.MemoBoardRepository;
        }
        public IMemoBoardRepository MemoBoardRepository { get; set; }

        [HttpGet]
        public IEnumerable<MemoBoard> FindAll(ulong owner = 0UL)
        {
            var boards = MemoBoardRepository.FindAllByOwner(owner);
            return boards;
        }

        [HttpGet("{id}")]
        public IActionResult Get(ulong id, ulong owner = 0UL)
        {
            var fetched = MemoBoardRepository.GetItem(id, owner);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public void Post([FromBody] MemoBoard board, ulong owner = 0UL)
        {
            var suc = MemoBoardRepository.AddItem(board);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public void Put(ulong id, [FromBody] MemoBoard board)
        {
            var suc = MemoBoardRepository.ReplaceItem(board, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public int RemoveAll(ulong owner)
        {
            return MemoBoardRepository.RemoveAllByOwner(owner);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id, ulong owner = 0UL)
        {
            var deleted = MemoBoardRepository.DeleteItem(id, owner);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
