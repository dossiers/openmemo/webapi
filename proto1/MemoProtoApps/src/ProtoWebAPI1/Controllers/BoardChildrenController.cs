﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MemoProto.MemoData1.Managers;
using MemoProto.MemoData1.Repositories;
using MemoProto.CoreLib1.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MemoProto.ProtoWebAPI1.Controllers
{
    // [Route("api/board/{parent}/child/{child}")]
    [Route("api/[controller]")]
    public class BoardChildrenController : Controller
    {
        public BoardChildrenController(IRepositoryManager repositoryManager)
        {
            BoardChildRepository = repositoryManager.BoardChildRepository;
        }
        public IBoardChildRepository BoardChildRepository { get; set; }


        [HttpGet("parent/{parent}")]
        public IEnumerable<BoardChild> FindChildren(ulong parent)
        {
            var boardChildren = BoardChildRepository.FindChildren(parent);
            return boardChildren;
        }

        [HttpPost]
        public void Post([FromBody] BoardChild boardChild)
        {
            var suc = BoardChildRepository.AddBoardChild(boardChild);

            // tbd:
            if (suc) {

            } else {

            }
        }

        //[HttpPut("{id}")]
        //public void Put(ulong id, [FromBody] BoardChild boardChild)
        //{
        //    var suc = BoardChildRepository.ReplaceBoardChild(boardChild, true);

        //    // tbd:
        //    if (suc) {

        //    } else {

        //    }
        //}

        [HttpPut("board/{parentId}/child/{childId}")]
        public void Put(ulong parentId, ulong childId, [FromBody] BoardChild boardChild)
        {
            // assert parent == boardChild.ParentId && child = boardChild.ChildId
            var suc = BoardChildRepository.ReplaceBoardChild(boardChild, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public int RemoveAll(ulong parent)
        {
            return BoardChildRepository.RemoveByParent(parent);
        }

        //[HttpDelete("{id}")]
        //public void Delete(ulong parentId, ulong childId)
        //{
        //    BoardChildRepository.DeleteBoardChild(id);
        //}
        [HttpDelete("board/{parentId}/child/{childId}")]
        public IActionResult Delete(ulong parentId, ulong childId)
        {
            var deleted = BoardChildRepository.DeleteBoardChild(parentId, childId);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
