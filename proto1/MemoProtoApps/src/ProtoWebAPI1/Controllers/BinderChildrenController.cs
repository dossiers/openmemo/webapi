﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MemoProto.MemoData1.Managers;
using MemoProto.MemoData1.Repositories;
using MemoProto.CoreLib1.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MemoProto.ProtoWebAPI1.Controllers
{
    // [Route("api/board/{binderId}/board/{binderChild}")]
    [Route("api/[controller]")]
    public class BinderChildrenController : Controller
    {
        public BinderChildrenController(IRepositoryManager repositoryManager)
        {
            BinderChildRepository = repositoryManager.BinderChildRepository;
        }
        public IBinderChildRepository BinderChildRepository { get; set; }


        [HttpGet("binderId/{binderId}")]
        public IEnumerable<BinderChild> FindChildre(ulong binderId)
        {
            var binderChildfren = BinderChildRepository.FindMemoBoards(binderId);
            return binderChildfren;
        }

        [HttpPost]
        public void Post([FromBody] BinderChild binderChild)
        {
            var suc = BinderChildRepository.AddBinderChild(binderChild);

            // tbd:
            if (suc) {

            } else {

            }
        }

        //[HttpPut("{id}")]
        //public void Put(ulong id, [FromBody] BinderChild binderChild)
        //{
        //    var suc = BinderChildRepository.ReplaceBinderChild(binderChild, true);
        //    // tbd:
        //    if (suc) {
        //    } else {
        //    }
        //}

        [HttpPut("board/{binderIdId}/board/{binderChildId}")]
        public void Put(ulong binderIdId, ulong binderChildId, [FromBody] BinderChild binderChild)
        {
            // assert binderId == binderChild.ParentId && binderChild = binderChild.ChildId
            var suc = BinderChildRepository.ReplaceBinderChild(binderChild, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public int RemoveAll(ulong binderId)
        {
            return BinderChildRepository.RemoveByBinder(binderId);
        }

        //[HttpDelete("{id}")]
        //public void Delete(ulong binderIdId, ulong binderChildId)
        //{
        //    BinderChildRepository.DeleteBinderChild(id);
        //}
        [HttpDelete("board/{binderIdId}/board/{binderChildId}")]
        public IActionResult Delete(ulong binderIdId, ulong binderChildId)
        {
            var deleted = BinderChildRepository.DeleteBinderChild(binderIdId, binderChildId);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
