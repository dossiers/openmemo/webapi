﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MemoProto.MemoData1.Managers;
using MemoProto.MemoData1.Repositories;
using MemoProto.CoreLib1.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MemoProto.ProtoWebAPI1.Controllers
{
    [Route("api/board/{board}/[controller]")]
    public class MemoShardsController : Controller
    {
        public MemoShardsController(IRepositoryManager repositoryManager)
        {
            MemoShardRepository = repositoryManager.MemoShardRepository;
        }
        public IMemoShardRepository MemoShardRepository { get; set; }

        [HttpGet]
        public IEnumerable<MemoShard> FindAll(ulong board = 0UL)
        {
            var shards = MemoShardRepository.FindAll(board);
            return shards;
        }

        [HttpGet("{id}")]
        public IActionResult Get(ulong id, ulong board = 0UL)
        {
            var fetched = MemoShardRepository.GetItem(id, board);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public void Post([FromBody] MemoShard shard, ulong board = 0UL)
        {
            var suc = MemoShardRepository.AddItem(shard);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public void Put(ulong id, [FromBody] MemoShard shard)
        {
            var suc = MemoShardRepository.ReplaceItem(shard, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public int RemoveAll(ulong board)
        {
            return MemoShardRepository.RemoveAll(board);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id, ulong board = 0UL)
        {
            var deleted = MemoShardRepository.DeleteItem(id, board);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}
