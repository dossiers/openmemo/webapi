﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MemoProto.MemoData1.Managers;
using MemoProto.MemoData1.Repositories;
using MemoProto.CoreLib1.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MemoProto.ProtoWebAPI1.Controllers
{
    [Route("api/user/{owner}/[controller]")]
    public class MemoBindersController : Controller
    {
        public MemoBindersController(IRepositoryManager repositoryManager)
        {
            MemoBinderRepository = repositoryManager.MemoBinderRepository;
        }
        public IMemoBinderRepository MemoBinderRepository { get; set; }

        [HttpGet]
        public IEnumerable<MemoBinder> FindAll(ulong owner = 0UL)
        {
            var binders = MemoBinderRepository.FindAllByOwner(owner);
            return binders;
        }

        [HttpGet("{id}")]
        public IActionResult Get(ulong id, ulong owner = 0UL)
        {
            var fetched = MemoBinderRepository.GetItem(id, owner);
            if (fetched == null) {
                return NotFound();
            } else {
                return new ObjectResult(fetched);   // ???
            }
        }

        [HttpPost]
        public void Post([FromBody] MemoBinder binder, ulong owner = 0UL)
        {
            var suc = MemoBinderRepository.AddItem(binder);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpPut("{id}")]
        public void Put(ulong id, [FromBody] MemoBinder binder)
        {
            var suc = MemoBinderRepository.ReplaceItem(binder, true);

            // tbd:
            if (suc) {

            } else {

            }
        }

        [HttpDelete]
        public int RemoveAll(ulong owner)
        {
            return MemoBinderRepository.RemoveAllByOwner(owner);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(ulong id, ulong owner = 0UL)
        {
            var deleted = MemoBinderRepository.DeleteItem(id, owner);
            if (deleted == null) {
                return NotFound();
            } else {
                return new ObjectResult(deleted);   // ???
            }
        }
    }
}