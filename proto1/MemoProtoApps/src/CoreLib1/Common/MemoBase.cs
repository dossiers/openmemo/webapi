﻿using MemoProto.CoreLib1.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.CoreLib1.Common
{
    public abstract class MemoBase : MemoElement
    {
        public ulong Owner { get; set; }
        public AccessLevel AccessLevel { get; set; }
        public BoardSize Size { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        // ... 

    }
}
