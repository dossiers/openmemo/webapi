﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.CoreLib1.Common
{
    public abstract class MemoElement
    {
        public ulong Id { get; set; }

        public long Created { get; set; }
        public long Updated { get; set; }
        public long Deleted { get; set; }
    }
}
