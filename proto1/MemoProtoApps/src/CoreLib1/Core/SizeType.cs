﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.CoreLib1.Core
{
    // Predefined Board Size
    public enum SizeType
    {
        Unknown = 0,
        Custom = 1,    // ???

        // Square shapes only??
        TinyTinySquare = 100,
        TinySquare = 200,
        SmallSquare = 300,
        MediumSquare = 400,
        LargeSquare = 500,
        ExLargeSquare = 600,
        ExExLargeSquare = 700,
        ExExExLargeSquare = 800,
        ExExExExLargeSquare = 900,
        ExExExExExLargeSquare = 1000,

        // etc...

        // infinite size ???

    }
}
