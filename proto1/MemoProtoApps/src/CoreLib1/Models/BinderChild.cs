﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.CoreLib1.Models
{
    public class BinderChild
    {
        // Parent's type should be MemoBinder
        // public ulong ParentId { get; set; }
        public ulong BinderId { get; set; }
        // Child should be MemoBoard.
        // public ulong ChildId { get; set; }
        public ulong BoardId { get; set; }

        // For convenience...
        public string BoardTitle { get; set; }
        
        // public BoardSize ChildSize { get; set; }  // Child's size is always the same as the parent.  --> TBD: How to enforce it???
        // public int ChildZOrder { get; set; }
        public int BoardZOrder { get; set; }
        // tbd:
        // overrideable access level, etc. ....
        // ... 

        public long Created { get; set; }
        public long Updated { get; set; }
        public long Deleted { get; set; }
    }
}
