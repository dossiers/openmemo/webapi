﻿using MemoProto.CoreLib1.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.CoreLib1.Models
{
    // Represents MemoBoard - Child Board relationship.
    public class BoardChild
    {
        // Parent's type should be MemoBoard
        public ulong ParentId { get; set; }
        // Child can be either MemoBoard or MemoBinder.
        public ulong ChildId { get; set; }

        // For convenience...
        public string ChildTitle { get; set; }

        // public BoardSize ChildSize { get; set; }  // Resize the child???
        public ElementPosition ChildPosition { get; set; }
        public int ChildZOrder { get; set; }
        // tbd:
        // overrideable access level, etc. ....
        // ... 

        public long Created { get; set; }
        public long Updated { get; set; }
        public long Deleted { get; set; }
    }
}
