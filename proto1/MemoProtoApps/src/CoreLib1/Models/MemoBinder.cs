﻿using MemoProto.CoreLib1.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.CoreLib1.Models
{
    public class MemoBinder : MemoBase
    {
        // The first MemoBoard.
        // This should be part of BinderChild records...
        //   ( --> How to enforce that? 
        //   ( Maybe, consider this as an extra board, if it is not present in the collection?
        //   ( If it is in the collection, just use it?
        // ...
        // The z-order of HomeBoard is infinite (regardless of the value defined in BinderChild.
        public ulong HomeBoard { get; set; }

        // ...
    }
}
