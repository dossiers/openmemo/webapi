﻿using MemoProto.CoreLib1.Common;
using MemoProto.CoreLib1.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.CoreLib1.Models
{
    public class MemoShard : MemoElement
    {
        public ulong Board { get; set; }   // Shard belongs to a single parent MemoBoard.
        // public ulong Owner { get; set; }   // owner information inherits from the Board.Owner.
        public ShardType Type { get; set; }
        // size ???
        public ElementPosition Position { get; set; }
        public int ZOrder { get; set; }
        // ...

        // tbd:
        // how to store the actual content?
        // ....

    }
}
