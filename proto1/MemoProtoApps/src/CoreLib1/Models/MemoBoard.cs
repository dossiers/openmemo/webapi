﻿using MemoProto.CoreLib1.Common;
using MemoProto.CoreLib1.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.CoreLib1.Models
{
    public class MemoBoard : MemoBase
    {
        public bool IsTerminal { get; set; } = false;   // If true, it cannot have child boards.
        // Used when it is included in another (parent) board???
        // Just use Title???
        // public string Synopsis { get; set; }

        // tbd:
        // how to specify parent board(1) - child board(n) relationship ???
        // child board location, z-order,  overrideable access level, ....
        // ... 
    }
}
