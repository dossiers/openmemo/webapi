﻿using MemoProto.CoreLib1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.MemoData1.Repositories
{
    public interface IBinderChildRepository
    {
        IEnumerable<BinderChild> FindMemoBoards(ulong binderId);
        // IEnumerable<BinderChild> FindMemoBinders(ulong boardId);

        bool AddBinderChild(BinderChild binderChild);
        // BinderChild CreateBinderChild(ulong binderId, ulong boardId, int zOrder = 0);

        bool ReplaceBinderChild(BinderChild binderChild, bool createIfNotExist = false);
        // BinderChild UpdateChildPosition(ulong binderId, ulong boardId);
        // BinderChild UpdateChildZOrder(ulong binderId, ulong boardId, int zOrder);

        BinderChild DeleteBinderChild(ulong binderId, ulong boardId);
        int RemoveByBinder(ulong binderId);
        // int RemoveByBoard(ulong boardId);

    }
}
