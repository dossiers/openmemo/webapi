﻿using MemoProto.CoreLib1.Common;
using MemoProto.CoreLib1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MemoProto.MemoData1.Repositories
{
    public interface IBoardChildRepository
    {
        IEnumerable<BoardChild> FindChildren(ulong parent);
        // IEnumerable<BoardChild> FindParents(ulong child);

        bool AddBoardChild(BoardChild boardChild);
        // BoardChild CreateBoardChild(ulong parentId, ulong childId, ElementPosition ChildPosition, int zOrder = 0);

        bool ReplaceBoardChild(BoardChild boardChild, bool createIfNotExist = false);
        // BoardChild UpdateChildPosition(ulong parentId, ulong childId, ElementPosition ChildPosition);
        // BoardChild UpdateChildZOrder(ulong parentId, ulong childId, int zOrder);

        BoardChild DeleteBoardChild(ulong parentId, ulong childId);
        int RemoveByParent(ulong parentId);
        // int RemoveByChild(ulong childId);

    }
}
