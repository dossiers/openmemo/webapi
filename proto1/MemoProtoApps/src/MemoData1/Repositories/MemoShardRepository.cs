﻿using MemoProto.CoreLib1.Common;
using MemoProto.CoreLib1.Core;
using MemoProto.CoreLib1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.MemoData1.Repositories
{
    public interface IMemoShardRepository
    {
        MemoShard GetItem(ulong id, ulong board = 0UL);
        IEnumerable<MemoShard> FindAll(ulong board);
        // IEnumerable<MemoShard> FindByType(ulong board, ShardType type);

        // How/where to store actual content ???
        bool AddItem(MemoShard shard);
        // MemoShard CreateItem(ulong board, ShardType type, ElementPosition position, int zOrder = 0);

        // "Overwrite" syntax. If the item does not exist, create one. ?? Or, ignore and return null ??
        bool ReplaceItem(MemoShard memoShard, bool createIfNotExist = false);
        // MemoShard UpdatePosition(ulong id, ElementPosition position);
        // MemoShard UpdateZOrder(ulong id, int zOrder);

        MemoShard DeleteItem(ulong id, ulong board = 0UL);
        int RemoveAll(ulong board);
        // int RemoveByType(ulong board, ShardType type);

    }
}
