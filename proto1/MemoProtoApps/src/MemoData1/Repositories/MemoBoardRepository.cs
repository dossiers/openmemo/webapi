﻿using MemoProto.CoreLib1.Common;
using MemoProto.CoreLib1.Core;
using MemoProto.CoreLib1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.MemoData1.Repositories
{
    public interface IMemoBoardRepository
    {
        MemoBoard GetItem(ulong id, ulong owner = 0UL);
        // IEnumerable<MemoBoard> FindAll();
        IEnumerable<MemoBoard> FindAllByOwner(ulong owner);
        // IEnumerable<MemoBoard> FindByAccessLevel(AccessLevel level);
        // IEnumerable<MemoBoard> FindByOwnerAndAccessLevel(ulong owner, AccessLevel level);

        // How/where to store actual content ???
        bool AddItem(MemoBoard board);
        // MemoBoard CreateItem(ulong owner, AccessLevel level, BoardSize size, string title = null, string description = null, bool isTerminal = false);

        // "Overwrite" semantics If the item does not exist, create one. ?? Or, ignore and return null ??
        bool ReplaceItem(MemoBoard memoBoard, bool createIfNotExist = false);
        // MemoBoard UpdateAccessLevel(ulong id, AccessLevel level);
        // MemoBoard UpdateTitle(ulong id, string title);

        MemoBoard DeleteItem(ulong id, ulong owner = 0UL);
        int RemoveAllByOwner(ulong owner);
        // int RemoveByOwnerAndAccessLevel(ulong owner, AccessLevel level);

    }
}
