﻿using MemoProto.CoreLib1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.MemoData1.DB
{
    public class MemoDbContext : DbContext
    {
        public MemoDbContext(DbContextOptions<MemoDbContext> options)
            : base(options)
        {
        }

        public DbSet<MemoBinder> MemoBinders { get; set; }
        public DbSet<MemoBoard> MemoBoards { get; set; }
        public DbSet<MemoShard> MemoShards { get; set; }
        public DbSet<BinderChild> BinderChildren { get; set; }
        public DbSet<BoardChild> BoardChildren { get; set; }

    }
}
