﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoProto.MemoData1.Repositories;
using MemoProto.MemoData1.Memory;

namespace MemoProto.MemoData1.Managers.Impl
{
    public sealed class MemoryRepositoryManager : IRepositoryManager
    {
        public MemoryRepositoryManager()
        {
        }

        private IBinderChildRepository binderChildRepository = new MemoryBinderChildRepository();
        private IBoardChildRepository boardChildRepository = new MemoryBoardChildRepository();
        private IMemoBinderRepository memoBinderRepository = new MemoryMemoBinderRepository();
        private IMemoBoardRepository memoBoardRepository = new MemoryMemoBoardRepository();
        private IMemoShardRepository memoShardRepository = new MemoryMemoShardRepository();


        public IBinderChildRepository BinderChildRepository
        {
            get
            {
                return binderChildRepository;
            }
        }

        public IBoardChildRepository BoardChildRepository
        {
            get
            {
                return boardChildRepository;
            }
        }

        public IMemoBinderRepository MemoBinderRepository
        {
            get
            {
                return memoBinderRepository;
            }
        }

        public IMemoBoardRepository MemoBoardRepository
        {
            get
            {
                return memoBoardRepository;
            }
        }

        public IMemoShardRepository MemoShardRepository
        {
            get
            {
                return memoShardRepository;
            }
        }
    }
}
