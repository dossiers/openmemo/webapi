﻿using MemoProto.MemoData1.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoProto.MemoData1.Managers
{
    public interface IRepositoryManager
    {
        IMemoBinderRepository MemoBinderRepository { get; }
        IBinderChildRepository BinderChildRepository { get; }
        IMemoBoardRepository MemoBoardRepository { get; }
        IBoardChildRepository BoardChildRepository { get; }
        IMemoShardRepository MemoShardRepository { get; }

    }
}
