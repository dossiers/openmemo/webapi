﻿using MemoProto.MemoData1.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoProto.CoreLib1.Common;
using MemoProto.CoreLib1.Core;
using MemoProto.CoreLib1.Models;
using System.Collections.Concurrent;

namespace MemoProto.MemoData1.Memory
{
    public class MemoryMemoShardRepository : IMemoShardRepository
    {
        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, MemoShard>> shardMap = new ConcurrentDictionary<ulong, IDictionary<ulong, MemoShard>>();


        public MemoShard GetItem(ulong id, ulong board = 0)
        {
            if(board > 0UL) {
                IDictionary<ulong, MemoShard> map;
                if (shardMap.TryGetValue(board, out map)) {
                    if (map.ContainsKey(id)) {
                        var memoShard = map[board];
                        return memoShard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public IEnumerable<MemoShard> FindAll(ulong board)
        {
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryGetValue(board, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }

        //public IEnumerable<MemoShard> FindByType(ulong board, ShardType type)
        //{
        //    throw new NotImplementedException();
        //}


        public bool AddItem(MemoShard shard)
        {
            var id = shard.Id;
            var board = shard.Board;
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryGetValue(board, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the id across different boards....
                if (map.ContainsKey(id)) {
                    System.Diagnostics.Debug.WriteLine($"MemoShard cannot be added because an item with the given id already exists: id = {id}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, MemoShard>();
                shardMap[board] = map;
            }

            // TBD: Update the Created/Updated field?
            map[id] = shard;
            return true;
        }

        public bool ReplaceItem(MemoShard memoShard, bool createIfNotExist = false)
        {
            var id = memoShard.Id;
            var board = memoShard.Board;
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryGetValue(board, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, MemoShard>();
                    shardMap[board] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[id] = memoShard;
            return true;
        }


        public MemoShard DeleteItem(ulong id, ulong board = 0)
        {
            if (board > 0UL) {
                IDictionary<ulong, MemoShard> map;
                if (shardMap.TryGetValue(board, out map)) {
                    var memoShard = map[id];
                    if (map.Remove(id)) {
                        return memoShard;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public int RemoveAll(ulong board)
        {
            IDictionary<ulong, MemoShard> map;
            if (shardMap.TryRemove(board, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

        //public int RemoveByType(ulong board, ShardType type)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
