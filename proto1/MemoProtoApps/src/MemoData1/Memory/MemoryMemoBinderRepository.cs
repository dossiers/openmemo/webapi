﻿using MemoProto.MemoData1.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoProto.CoreLib1.Common;
using MemoProto.CoreLib1.Core;
using MemoProto.CoreLib1.Models;
using System.Collections.Concurrent;

namespace MemoProto.MemoData1.Memory
{
    public class MemoryMemoBinderRepository : IMemoBinderRepository
    {
        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, MemoBinder>> memoBinderMap = new ConcurrentDictionary<ulong, IDictionary<ulong, MemoBinder>>();


        public MemoBinder GetItem(ulong id, ulong owner = 0UL)
        {
            if (owner > 0UL) {
                IDictionary<ulong, MemoBinder> map;
                if (memoBinderMap.TryGetValue(owner, out map)) {
                    if (map.ContainsKey(id)) {
                        var memoBinder = map[owner];
                        return memoBinder;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public IEnumerable<MemoBinder> FindAllByOwner(ulong owner)
        {
            IDictionary<ulong, MemoBinder> map;
            if (memoBinderMap.TryGetValue(owner, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }


        public bool AddItem(MemoBinder memoBinder)
        {
            var id = memoBinder.Id;
            var owner = memoBinder.Owner;
            IDictionary<ulong, MemoBinder> map;
            if (memoBinderMap.TryGetValue(owner, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the id across different owners....
                if (map.ContainsKey(id)) {
                    System.Diagnostics.Debug.WriteLine($"MemoBinder cannot be added because an item with the given id already exists: id = {id}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, MemoBinder>();
                memoBinderMap[owner] = map;
            }

            // TBD: Update the Created/Updated field?
            map[id] = memoBinder;
            return true;
        }

        public bool ReplaceItem(MemoBinder memoBinder, bool createIfNotExist = false)
        {
            var id = memoBinder.Id;
            var owner = memoBinder.Owner;
            IDictionary<ulong, MemoBinder> map;
            if (memoBinderMap.TryGetValue(owner, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, MemoBinder>();
                    memoBinderMap[owner] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[id] = memoBinder;
            return true;
        }


        public MemoBinder DeleteItem(ulong id, ulong owner = 0UL)
        {
            if (owner > 0UL) {
                IDictionary<ulong, MemoBinder> map;
                if (memoBinderMap.TryGetValue(owner, out map)) {
                    var memoBinder = map[id];
                    if (map.Remove(id)) {
                        return memoBinder;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                throw new NotImplementedException();
            }
        }

        public int RemoveAllByOwner(ulong owner)
        {
            IDictionary<ulong, MemoBinder> map;
            if (memoBinderMap.TryRemove(owner, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

        //public int RemoveByType(ulong owner, ShardType type)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
