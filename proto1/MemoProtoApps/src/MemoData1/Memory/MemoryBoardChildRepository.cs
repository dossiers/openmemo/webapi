﻿using MemoProto.MemoData1.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoProto.CoreLib1.Common;
using MemoProto.CoreLib1.Models;
using System.Collections.Concurrent;

namespace MemoProto.MemoData1.Memory
{
    public class MemoryBoardChildRepository : IBoardChildRepository
    {
        private static readonly ConcurrentDictionary<ulong, IDictionary<ulong, BoardChild>> boardChildMap = new ConcurrentDictionary<ulong, IDictionary<ulong, BoardChild>>();

        public IEnumerable<BoardChild> FindChildren(ulong parentId)
        {
            IDictionary<ulong, BoardChild> map;
            if (boardChildMap.TryGetValue(parentId, out map)) {
                var collection = map.Values;
                return collection;
            } else {
                return null;
            }
        }

        public bool AddBoardChild(BoardChild boardChild)
        {
            var parentId = boardChild.ParentId;
            var childId = boardChild.ChildId;
            IDictionary<ulong, BoardChild> map;
            if (boardChildMap.TryGetValue(parentId, out map)) {
                // tbd:
                // This does not gurantee the uniqueness of the childId across different parentIds....
                if (map.ContainsKey(childId)) {
                    System.Diagnostics.Debug.WriteLine($"BoardChild cannot be added because an item with the given childId already exists: childId = {childId}.");
                    return false;
                }
            } else {
                map = new Dictionary<ulong, BoardChild>();
                boardChildMap[parentId] = map;
            }

            // TBD: Update the Created/Updated field?
            map[childId] = boardChild;
            return true;
        }

        public bool ReplaceBoardChild(BoardChild boardChild, bool createIfNotExist = false)
        {
            var parentId = boardChild.ParentId;
            var childId = boardChild.ChildId;
            IDictionary<ulong, BoardChild> map;
            if (boardChildMap.TryGetValue(parentId, out map)) {
            } else {
                if (createIfNotExist) {
                    map = new Dictionary<ulong, BoardChild>();
                    boardChildMap[parentId] = map;
                } else {
                    return false;
                }
            }

            // TBD: Update the Updated field?
            map[childId] = boardChild;
            return true;
        }

        public BoardChild DeleteBoardChild(ulong parentId, ulong childId)
        {
            IDictionary<ulong, BoardChild> map;
            if (boardChildMap.TryGetValue(parentId, out map)) {
                var boardChild = map[childId];
                if (map.Remove(childId)) {
                    return boardChild;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        public int RemoveByParent(ulong parentId)
        {
            IDictionary<ulong, BoardChild> map;
            if (boardChildMap.TryRemove(parentId, out map)) {
                return map.Count;
            } else {
                return 0;
            }
        }

    }
}
